# VoyBinocVision
Required Supporting Software:
PCL 1.8.1 (https://github.com/PointCloudLibrary/pcl/releases)
CMAke 3.12 (https://cmake.org/download/)
Vimba SDK (https://www.alliedvision.com/en/products/software.html)
OpenCV (https://opencv.org/releases.html)

MAke sure that all installs are for windows 10 x64

ENVironment variables that need to be set (update based on your system install locations):
OPENCV_DIR (C:\OpenSourceSoft\opencv\build)
VIMBA_HOME (C:\OpenSourceSoft\Vimba_2.1)
