#include "Search_2D.h"

// **********************************************************************************************  //
//    Pipeline			                                                                               //
//																										                                             //
// **********************************************************************************************  //

//	Circle Pipeline Full
//
std::vector<circle2i> Search_2D::circlePipelineFullImage(cv::Mat& image,const cv::Mat maskMat,
	std::vector<trackedTarget> currentTargets)
{	
	// Settings
	bool open = false;	
	bool removeLines = false;
	int radMin = 20;
	int radMax = 55;

	// Variables		
	std::vector<circle2i> circles2i;	
	Mat smoothed, edges, targetsMask;

	blur(image, smoothed, Size(4, 4)); // 100	
	Canny(smoothed, edges, cannyHigh, cannyLow, 3, false);	// 1000

	BlobDetect bD;
	bD.detect(smoothed);
	bD.maskBlobs(edges, 3);

	if (open)
	{
		opening(image);
		dilatedDifference(thresholdMat, opened);
		edges.copyTo(edges, difMat);
	}
	
	//To Do I lost remove Lines somewhere
	edges.copyTo(edges, maskMat);			
	//targetsMask = Mat(image.rows, image.cols, CV_8UC1, 255);

	for (int i = 0; i < currentTargets.size(); i++)
	{
		targetMask(edges, currentTargets[i].lastLocation);
	}	
	
	//edges.copyTo(edges, targetsMask);

	//imshow("edges", edges);
	//waitKey(1);

// Go through Hough, get maxPeaks
	for (int r = radMin; r <= radMax; r++)
	{
		Mat tempHough = getHoughSpace(edges, r);		
		bool morePeaks = true;
		while(morePeaks) 
		{
			circle2i iCirc = maxPeak(tempHough);			
			if ( (int)iCirc.mag > 2 * 3 * 0.40 * r)
			{
				iCirc.rad = r;
				circles2i.push_back(iCirc);
				targetMask(tempHough, iCirc);

				//imshow("tempHough", tempHough);
				//waitKey(1);
			} 
			else { morePeaks = false; }
		}
	}
	if (circles2i.empty()) { return circles2i; }

	// Sort by mag ascending
	// https://stackoverflow.com/questions/5174115/sorting-a-vector-of-objects-by-a-property-of-the-object/19165859
	std::sort(circles2i.begin(), circles2i.end(), [](circle2i a, circle2i b) {return a.mag > b.mag; });
	
	// Trim circles within r/2 box
	for (std::vector<circle2i>::iterator it1 = circles2i.begin(); it1 != circles2i.end(); it1++)
	{
		int rh = it1->rad / 2;
		for (std::vector<circle2i>::iterator it2 = it1 + 1; it2 != circles2i.end(); /*it2++*/)
		{
			int dx = abs(it1->x - it2->x);
			int dy = abs(it1->y - it2->y);
			if (dx < rh || dy < dy)
			{
				it2 = circles2i.erase(it2);
			}
			else 
			{
				it2++;
			}
		}
	}


	return inOutCircleCheck(image, circles2i);
	//return circles2i;
}

//	Circle Pipeline ROI
//
//
circle2D Search_2D::circlePipelineROI(cv::Mat& image, circle2D lastCirc, int rMin, int rMax)
{
	// Settings
	bool open = false;

	// Varibles		
	std::vector<circle2D> circles2D;	
	Mat smoothed, edges;
	struct houghAnswer {
		circle2i center;
		Mat houghMat;
	};
	std::map<int, houghAnswer> houghSpaces;

	int lastRad = round(lastCirc.rad);

	blur(image, smoothed, Size(3, 3)); // 100
	Canny(smoothed, edges, cannyHigh, cannyLow, 3, false);	// 1000	

	// ToDo Search out from last circ.r stop when intensity drops.
	// Go through Hough, get maxPeak	
	circle2i maxMag;
	for (int r = lastRad; r <= rMax; r++)
	{
		Mat tempHough = getHoughSpace(edges, r);
		circle2i iCirc = maxPeak(tempHough);
		//if ((int)iCirc.mag > minQuality * r)
		{
			iCirc.rad = r;
			houghSpaces[r] = houghAnswer{ iCirc, tempHough };
			if (iCirc.mag > maxMag.mag)
			{
				maxMag = iCirc;
			}
			else
			{
				break;
			}
		}
	}

	for (int r = lastRad - 1; r >= rMin; r--)
	{
		Mat tempHough = getHoughSpace(edges, r);
		circle2i iCirc = maxPeak(tempHough);
		//if ((int)iCirc.mag > minQuality * r)
		{
			iCirc.rad = r;
			houghSpaces[r] = houghAnswer{ iCirc, tempHough };
			if (iCirc.mag > maxMag.mag)
			{
				maxMag = iCirc;
			}
			else
			{
				break;
			}
		}
	}

	//// Speed Tests
	//long b = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - lastUpdate).count());
	//lastUpdate = std::chrono::steady_clock::now();

	
	if (houghSpaces.empty()) { return circle2D(); }
	int avePixIntensity = 0;
	int percentAbove;
	percentAbove = checkCircle(image, cv::Point2i(maxMag.x, maxMag.y), maxMag.rad - 10, cannyLow, avePixIntensity);
	if (percentAbove < 55) { return circle2D(); }
	int outBallThreshold = avePixIntensity - 30;
	percentAbove = checkCircle(image, cv::Point2i(maxMag.x, maxMag.y), maxMag.rad + 2, outBallThreshold, avePixIntensity);
	if (percentAbove > 25) { return circle2D(); }
	


	circles2D.push_back(averagedCenter(houghSpaces[maxMag.rad].center, houghSpaces[maxMag.rad].houghMat, 3));
	if (houghSpaces.count(maxMag.rad - 1))
	{ circles2D.push_back(houghSpaces[maxMag.rad - 1].center); }
	if (houghSpaces.count(maxMag.rad + 1))
	{ circles2D.push_back(houghSpaces[maxMag.rad + 1].center); }

	double radAvg = 0;
	double magTot = 0;

	for (int i = 0; i < circles2D.size(); i++)
	{
		magTot += circles2D[i].mag;
		radAvg += (circles2D[i].rad * circles2D[i].mag);
	}

	radAvg /= magTot;
	circles2D[0].rad = radAvg;

	if (circles2D[0].mag == 0)
	{
		return circle2D();
	}

	return circles2D[0];

	//// Speed Tests
	//long c = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - lastUpdate).count());
	//lastUpdate = std::chrono::steady_clock::now();
}

// **********************************************************************************************  //
//    Mat Functions	                                                                               //
//																										                                             //
// **********************************************************************************************  //

//	Draw Circle
//see https://www.thecrazyprogrammer.com/2016/12/bresenhams-midpoint-circle-algorithm-c-c.html for algo reference
void Search_2D::drawCircle(Mat& image, cv::Point2f center, double radius, int val)
{
	std::vector<cv::Point2f> offPoints;
	float x0 = center.x;
	float y0 = center.y;
	float rad = radius;
	float x = radius;
	float y = 0;
	float err = 0;

	while (x >= y)
	{
		offPoints.push_back(cv::Point2f(x0 + x, y0 + y));
		offPoints.push_back(cv::Point2f(x0 + y, y0 + x));
		offPoints.push_back(cv::Point2f(x0 - y, y0 + x));
		offPoints.push_back(cv::Point2f(x0 - x, y0 + y));
		offPoints.push_back(cv::Point2f(x0 - x, y0 - y));
		offPoints.push_back(cv::Point2f(x0 - y, y0 - x));
		offPoints.push_back(cv::Point2f(x0 + y, y0 - x));
		offPoints.push_back(cv::Point2f(x0 + x, y0 - y));

		if (err <= 0)
		{
			y += 1;
			err += 2 * y + 1;
		}

		if (err > 0)
		{
			x -= 1;
			err -= 2 * x + 1;
		}
	}
	drawOffPoints(image, offPoints, val);
}

void Search_2D::printMat(Mat image)
{
	for (int i = 0; i < image.rows; i++)
	{
		for (int j = 0; j < image.cols; j++)
		{
			std::cout << std::to_string(image.at<uchar>(i, j)) << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void Search_2D::inBoundsPoint(Point& iBP, Mat& image)
{
	iBP.x = std::min(iBP.x, image.cols - 1);
	iBP.x = std::max(iBP.x, 0);
	iBP.y = std::min(iBP.y, image.rows - 1);
	iBP.y = std::max(iBP.y, 0);
}

// **********************************************************************************************  //
//    Alternate Circle Search                                                                      //
//																										                                             //
// **********************************************************************************************  //

// Set Off Points
void Search_2D::drawOffPoints(Mat& image, std::vector<cv::Point2f> offPoints, int colorVal)
{
	std::vector<cv::Point2i> shadePts(4);
	std::vector<int> shadeColor(4);
	for (int i = 0; i < offPoints.size(); i++)
	{
		int Xi = std::floor(offPoints[i].x);
		int Xii = Xi + 1;
		double Xdii = offPoints[i].x - Xi;
		double Xdi = 1 - Xdii;

		int Yi = std::floor(offPoints[i].y);
		int Yii = Yi + 1;
		double Ydii = offPoints[i].y - Yi;
		double Ydi = 1 - Ydii;

		shadePts[0] = cv::Point2i(Xi, Yi);
		shadeColor[0] = Ydi * Xdi * colorVal;
		shadePts[1] = cv::Point2i(Xii, Yi);
		shadeColor[1] = Ydi * Xdii * colorVal;
		shadePts[2] = cv::Point2i(Xi, Yii);
		shadeColor[2] = Ydii * Xdi * colorVal;
		shadePts[3] = cv::Point2i(Xii, Yii);
		shadeColor[3] = Ydii * Xdii * colorVal;

		for (int j = 0; j < 4; j++)
		{
			if (shadePts[j].x >= 0 && shadePts[j].x < image.cols && shadePts[j].y >= 0 && shadePts[j].y < image.rows)
			{
				image.at<cv::uint8_t>(shadePts[j].y, shadePts[j].x) = (uchar)std::min(image.at<cv::uint8_t>(shadePts[j].y, shadePts[j].x) + shadeColor[j], 255);
			}
		}
	}
}

// **********************************************************************************************  //
//    Alternate Circle Search                                                                      //
//																										                                             //
// **********************************************************************************************  //	

// Stolen from: https://github.com/sdg002/RANSAC/blob/master/Algorithm/BullockCircleFitting.py
circle2D Search_2D::bullockCircleFit(std::vector<cv::Point2i> circPoints)
{
	std::vector<cv::Point2d> shiftedPts;
	Point2d meanPt;

	//computeMean()
	{
		int X = 0;
		int Y = 0;

		for (int i = 0; i < circPoints.size(); i++)
		{
			X += circPoints[i].x;
			Y += circPoints[i].y;
		}

		//X /= circPoints.size();
		//Y /= circPoints.size();
		meanPt.x = (double)X / circPoints.size();
		meanPt.y = (double)Y / circPoints.size();
	}

	//shiftAllPoints()
	{
		shiftedPts.resize(circPoints.size());
		for (int i = 0; i < circPoints.size(); i++)
		{
			shiftedPts[i].x = circPoints[i].x - meanPt.x;
			shiftedPts[i].y = circPoints[i].y - meanPt.y;
		}
	}

	double Su = 0, Sv = 0, Suu = 0, Svv = 0, Suv = 0, Suuu = 0, Svvv = 0, Suvv = 0, Svuu = 0;
	for (int i = 0; i < shiftedPts.size(); i++)
	{
		double tSu = shiftedPts[i].x;
		double tSv = shiftedPts[i].y;
		Su += tSu;
		Sv += tSv;

		double tSuu = tSu * tSu;
		double tSvv = tSv * tSv;
		Suu += tSuu;
		Svv += tSvv;

		Suv += tSu * tSv;
		Svvv += tSvv * tSv;
		Suvv += tSu * tSvv;
		Svuu += tSv * tSuu;
	}

	double	C1 = 0.5 * (Suuu + Suvv);
	double	C2 = 0.5 * (Svvv + Svuu);
	double	Uc = (C2 * Suv - C1 * Svv) / (Suv * Suv - Suu * Svv);
	double	Vc = (C1 * Suv - C2 * Suu) / (Suv * Suv - Suu * Svv);
	double	alpha = Uc * Uc + Vc * Vc + (Suu + Svv) / circPoints.size();

	double	realX = meanPt.x + Uc;
	double	realY = meanPt.y + Vc;
	double	radius = sqrt(alpha);

	return circle2D(realX, realY, radius, 0);
};

// https://en.wikipedia.org/wiki/Random_sample_consensus		
circle2D Search_2D::searchCircleRansac(std::vector<Point2i> allPts)
{
	// Givens:

	//	n = 3;
	int k = 50;
	double t = 50.0; // Pixel
	//int d = 100; // 40% pix of 40 rad circle
	double d = 0.40; // Percent of circle
	if (allPts.size() < 3)
	{
		return circle2D();
		//std::cout << "Less Than three Points!";
	}

	double bestErr = 10000;
	circle2D bestModel;

	// Equation			

	// while iterations < k do
	for (int i = 0; i < k; i++)
	{
		// maybeInliers := n randomly selected values from data
		int a = 0;
		int b = 0;
		int c = 0;
		while (a == b || a == c || b == c)
		{
			a = rand() % allPts.size();
			b = rand() % allPts.size();
			c = rand() % allPts.size();
		}
		std::vector<Point2i> maybeInliers{ allPts[a], allPts[b], allPts[c] };
		// maybeModel: = model parameters fitted to maybeInliers
		circle2D maybeModel = bullockCircleFit(maybeInliers);
		// alsoInliers := empty set *nah, just going to use maybeInliers

		double rSquared = maybeModel.rad * maybeModel.rad;

		if (maybeModel.rad < 20 || maybeModel.rad > 55) { continue; }

		for (int j = 0; j < allPts.size(); j++)
		{
			if (j == a || j == b || j == c) { continue; }

			// if point fits maybeModel with an error smaller than t add point to maybeInliers
			double ptDistSquared = (maybeModel.x - allPts[j].x) * (maybeModel.x - allPts[j].x) +
				(maybeModel.y - allPts[j].y) * (maybeModel.y - allPts[j].y);
			//double ptDistSquared = pow(maybeModel.x - allPts[j].x, 2) + pow(maybeModel.y - allPts[j].y, 2);
			double tR = abs(rSquared - ptDistSquared);
			//std::cout << tR << std::endl;
			if (t >= abs(rSquared - ptDistSquared))
			{
				maybeInliers.push_back(allPts[j]);
			}
		}

		// if the number of elements in maybeInliers is > d then
		if (maybeInliers.size() > d * 2 * 3 * maybeModel.rad)//allPts.size() * d) 
		{
			// betterModel: = model parameters fitted to all points in maybeInliers and alsoInliers
			circle2D betterModel = bullockCircleFit(maybeInliers);
			// thisErr := a measure of how well betterModel fits these points
			double thisErr = fitError(maybeInliers, betterModel);
			// if thisErr < bestErr then -> bestFit: = betterModel,	bestErr : = thisErr
			if (thisErr < bestErr) {
				bestErr = thisErr;
				//std::cout << "Error: " << thisErr << std::endl;
				betterModel.mag = maybeInliers.size();
				bestModel = betterModel;
			}
		}
	}

	return bestModel;
};

//
// 
//
circle2D Search_2D::circleSearch(Mat cannyRoiMat)
{
	std::vector<Point2i> edgePoints;
	uchar* p;
	for (int j = 0; j < cannyRoiMat.rows; j++)
	{
		p = cannyRoiMat.ptr<uchar>(j);
		for (int i = 0; i < cannyRoiMat.cols; i++)
		{			
			if (p[i] != 0) { edgePoints.push_back(Point2i(j, i)); }
		}
	}

	//circle2D tC = bullockCircleFit(edgePoints);
	circle2D tC = searchCircleRansac(edgePoints);

	double tY = tC.x;
	tC.x = tC.y;
	tC.y = tY;
	return tC;
}


double Search_2D::fitError(std::vector<Point2i> checkPts, circle2D fitCirc)
{
	double rSquared = fitCirc.rad * fitCirc.rad;
	double error = 0;

	for (int j = 0; j < checkPts.size(); j++)
	{
		double ptDistSquared = pow(checkPts[j].x - fitCirc.x, 2) + pow(checkPts[j].y - fitCirc.y, 2);
		error += abs(rSquared - ptDistSquared);
	}
	error /= checkPts.size();
	return error;
}

// **********************************************************************************************  //
//    PipeLine Functions                                                                           //
//																										                                             //
// **********************************************************************************************  //
Mat Search_2D::getHoughSpace(Mat& edges, int rad) 
{
	Mat image = Mat(Size(edges.rows, edges.cols), CV_8UC1, Scalar(0));
	int incrementVal = 1;
	uchar* p;
	for (int y0 = 0; y0 < edges.rows; y0++)
	{
		p = edges.ptr<uchar>(y0);
		for (int x0 = 0; x0 < edges.cols; x0++)
		{
			//	Add Circle
			//see https://en.wikipedia.org/wiki/Midpoint_circle_algorithm for algo reference
			if (p[x0] != 0)			
			{
				int x = rad - 1;
				int y = 0;
				int dx = 1;
				int dy = 1;

				int err = dx - (rad << 1);

				int tempX;
				int tempY;

				while (x >= y)
				{
					tempX = x0 + x; tempY = y0 + y;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 + y; tempY = y0 + x;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 - y; tempY = y0 + x;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 - x; tempY = y0 + y;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 - x; tempY = y0 - y;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 - y; tempY = y0 - x;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 + y; tempY = y0 - x;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}
					tempX = x0 + x; tempY = y0 - y;
					if (tempX >= 0 && tempX < image.cols && tempY >= 0 && tempY < image.rows)
					{
						image.at<cv::uint8_t>(tempY, tempX) += incrementVal;
					}

					if (err <= 0)
					{
						y++;
						err += dy;
						dy += 2;
					}
					if (err > 0)
					{
						x--;
						dx += 2;
						err += dx - (rad << 1);
					}
				}				
			}
		}
	}
	return image;
}

circle2i Search_2D::maxPeak(Mat& houghImg) 
{
	circle2i foundPeak;
	uchar* p;
	for (int y = 0; y < houghImg.rows; y++)
	{
		p = houghImg.ptr<uchar>(y);
		for (int x = 0; x < houghImg.cols; x++)
		{
			if (p[x] > foundPeak.mag)
			{
				foundPeak.x = x;
				foundPeak.y = y;
				foundPeak.mag = p[x];
			}
		}
	}
	return foundPeak;
}

// Averages Center
//
circle2D Search_2D::averagedCenter(circle2i center, Mat& houghSpace, int avgBoxSize) {

	double xAverage = 0;
	double yAverage = 0;
	double totalMag = 0;

	int minX = std::max(0, (center.x - avgBoxSize));
	int maxX = std::min(houghSpace.cols - 1, (center.x + avgBoxSize));
	int minY = std::max(0, (center.y - avgBoxSize));
	int maxY = std::min(houghSpace.rows - 1, (center.y + avgBoxSize));

	for (int i = minX; i <= maxX; i++)
	{
		for (int j = minY; j <= maxY; j++)
		{
			int mag = houghSpace.at<cv::uint8_t>(j, i);
			xAverage += (double)(i * mag);// *mag;
			yAverage += (double)(j * mag);// *mag;
			totalMag += mag;// *mag;
		}
	}

	xAverage /= totalMag;
	yAverage /= totalMag;
	//double magAverage = totalMag / std::pow((2 * avgBoxSize + 1), 2);

	circle2D averagedCenter(xAverage, yAverage, center.rad, center.mag);
	return averagedCenter;
}

// **********************************************************************************************  //
//    Draw Helpers	                                                                               //
//																										                                             //
// **********************************************************************************************  //
void Search_2D::showDebugWindows(std::vector<circle2D> circles)
{

	cv::Mat circleOverlay1 = original.clone();
	for (int i = 0; i < circles.size(); i++)
	{
		drawCircle(circleOverlay1, cv::Point2i(circles.at(i).x, circles.at(i).y), circles.at(i).rad, 250);
	}

	cv::Mat circleOverlay2 = edges.clone();
	for (int i = 0; i < circles.size(); i++)
	{
		drawCircle(circleOverlay2, cv::Point2i(circles.at(i).x, circles.at(i).y), circles.at(i).rad, 250);
	}

	std::vector<Mat> allDebug{ original, thresholdMat, opened, difMat, smoothed, edges, circleOverlay1, circleOverlay2 };
	std::vector<std::string> allNames{ "original", "thresholdMat", "opened", "difMat", "smoothed", "edges", "circleOverlay1", "circleOverlay2" };

	for (int i = 0; i < allDebug.size(); i++)
	{
		if (!allDebug[i].empty())
		{
			namedWindow(allNames[i], WINDOW_AUTOSIZE);
			imshow(allNames[i], allDebug[i]);
		}
	}
	waitKey(2000);
}

// **********************************************************************************************  //
//    Tool functions                                                                               //
//																										                                             //
// **********************************************************************************************  //

//
//
cv::Mat Search_2D::getROI(const cv::Mat& totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax, int scaleFactor)
{
	int scaleX = (ptMax.x - ptMin.x) * (scaleFactor - 1) / 2; //gives how much padding to add to each side
	int scaleY = (ptMax.y - ptMin.y) * (scaleFactor - 1) / 2;

	ptMin.x = ptMin.x - scaleX;
	ptMax.x = ptMax.x + scaleX;
	ptMin.y = ptMin.y - scaleY;
	ptMax.y = ptMax.y + scaleY;

	//Dont allow for out of bounds
	ptMin.x = std::max(0, ptMin.x);
	ptMin.y = std::max(0, ptMin.y);
	ptMax.x = std::min(totalMat.cols - 1, ptMax.x);
	ptMax.y = std::min(totalMat.rows - 1, ptMax.y);

	cv::Rect roi(ptMin, ptMax);
	Mat subImg(totalMat, roi);
	return subImg;
}

void Search_2D::targetMask(Mat& maskMat, circle2i maskTarget)
{				
	circle(maskMat, Point(maskTarget.x, maskTarget.y), maskTarget.rad + 2, Scalar(0), CV_FILLED);
		//int rh = maskTarget.rad + 2;
		//int x = max(0, (maskTarget.x - rh));
		//int y = max(0, (maskTarget.y - rh));
		//int w = maskTarget.rad;//min(maskMat.cols - 1, (maskTarget.x + rh));
		//int h = maskTarget.rad;//min(maskMat.rows - 1, (maskTarget.y + rh));

		//cv::Rect rect(x, y, w, h);
		////cv::Rect rect(y, x, h, w);
		//rectangle(maskMat, rect, Scalar(0), FILLED, LINE_8);
}

//
//
void Search_2D::opening(const cv::Mat& originalMat, int numIterations, int elementSize, int thresholdMin, int thresholdMax)
{
	cv::Mat thresholdMat;
	threshold(originalMat, thresholdMat, thresholdMin, thresholdMax, CV_THRESH_BINARY_INV);

	Mat element = cv::getStructuringElement(cv::MORPH_CROSS,
		Size(2 * elementSize + 1, 2 * elementSize + 1),
		Point(elementSize, elementSize));

	cv::morphologyEx(thresholdMat, opened, cv::MORPH_OPEN, element, cv::Point(-1, -1), numIterations);
}

//
//
cv::Mat Search_2D::dilatedDifference(const cv::Mat& mat1, const cv::Mat& mat2, int kernelSize)
{
	Mat element = cv::getStructuringElement(cv::MORPH_CROSS,
		Size(2 * kernelSize + 1, 2 * kernelSize + 1),
		Point(kernelSize, kernelSize));

	cv::absdiff(mat1, mat2, difMat);
	cv::dilate(difMat, difMat, element);
	return difMat;
}

//	<summary>Checks outside is black, inside is white</summary>
//
std::vector<circle2i> Search_2D::inOutCircleCheck(Mat& image, std::vector<circle2i> circles)
{
	// Settings
	int MIN_BALL_INTENSITY = 150;
	// Varibles
	std::vector<circle2i> betterCircles;

	for (int i = 0; i < circles.size(); i++)
	{
		int avePixIntensity = 0;
		int percentAbove;
		if (circles[i].rad < 10) { continue; }

		// Inside circumference sweep should be > 55% of circle above MinBallIntensity
		percentAbove = checkCircle(image, cv::Point2i(circles[i].x, circles[i].y), circles[i].rad - 10, cannyLow, avePixIntensity);
		if (percentAbove > 55)
		{
			//  Outside circumference < 25% above threshold
			int outBallThreshold = avePixIntensity - 30;
			percentAbove = checkCircle(image, cv::Point2i(circles[i].x, circles[i].y), circles[i].rad + 2, outBallThreshold, avePixIntensity);
			if (percentAbove < 25)
			{
				betterCircles.push_back(circles[i]);
			}
			//else { std::cout << "Percent Above OutSide: " << percentAbove << " Ave intensity: " << avePixIntensity << std::endl; }
		}
		//else { std::cout << "Percent Above Inside: " << percentAbove << " Ave intensity: " << avePixIntensity << std::endl; }
	}
	return betterCircles;
}

// Check Points
void Search_2D::checkPoints(Mat& image, std::vector<cv::Point2i> diameterPix, int threshold, int& allPix, int& abovePix, int& allIntensity)
{
	for (int i = 0; i < diameterPix.size(); i++)
	{
		if (diameterPix[i].x >= 0 && diameterPix[i].x < image.cols && diameterPix[i].y >= 0 && diameterPix[i].y < image.rows)
		{
			allPix++;
			int pixIntensity = image.at<cv::uint8_t>(diameterPix[i].y, diameterPix[i].x);
			allIntensity += pixIntensity;
			if (pixIntensity > threshold) { abovePix++; }
		}
	}
}

//	Check Circle
int Search_2D::checkCircle(Mat& image, cv::Point2i center, int radius, int threshold, int& aveIntensity)
{
	std::vector<cv::Point2i> diameterPix;
	int x0 = std::round(center.x);
	int y0 = std::round(center.y);
	int rad = std::round(radius);
	int x = rad - 1;
	int y = 0;
	int dx = 1;
	int dy = 1;

	int err = dx - (rad << 1);

	int diameterPixCount = 0;
	int abovePixCount = 0;

	while (x >= y)
	{
		diameterPix.push_back(cv::Point2f(x0 + x, y0 + y));
		diameterPix.push_back(cv::Point2f(x0 + y, y0 + x));
		diameterPix.push_back(cv::Point2f(x0 - y, y0 + x));
		diameterPix.push_back(cv::Point2f(x0 - x, y0 + y));
		diameterPix.push_back(cv::Point2f(x0 - x, y0 - y));
		diameterPix.push_back(cv::Point2f(x0 - y, y0 - x));
		diameterPix.push_back(cv::Point2f(x0 + y, y0 - x));
		diameterPix.push_back(cv::Point2f(x0 + x, y0 - y));

		if (err <= 0)
		{
			y++;
			err += dy;
			dy += 2;
		}
		if (err > 0)
		{
			x--;
			dx += 2;
			err += dx - (rad << 1);
		}
		checkPoints(image, diameterPix, threshold, diameterPixCount, abovePixCount, aveIntensity);
	}
	aveIntensity = std::round((double)aveIntensity / diameterPixCount);


	return std::round((double)abovePixCount / diameterPixCount * 100);
}