#pragma once
//CPP libaries
#include <mutex>

//Included libraries
#include <opencv2/opencv.hpp>

//Local Libraries
#include "StorageObjects.h"
#include "SimpleViewer.h"
#include "StopWatch.h"


class RemoveLines
{
	private:
		bool debugMode;

		void dilate(cv::Mat& mat1,int iterations = 1, int kernelSize = 2)
		{
			for (int i = 0; i < iterations; i++)
			{
				Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
					Size(2 * kernelSize + 1, 2 * kernelSize + 1),
					Point(kernelSize, kernelSize));
				cv::dilate(mat1, mat1, element);

			}
		}

	public:

		RemoveLines(bool _debugMode = false)
		{
			debugMode = _debugMode;
		}
	
		//Given a matrix, finds all lines in the image. Returns a binarized mat of the found lines.
		//TODO: Could make static but then cant set debug mat
		static cv::Mat houghLineTransform(const cv::Mat& mat, double rhoRes = 1, int threshold = 30, double thetaDegRes = 0.05, double minLength = 25, double maxLineGap = 10, bool debug = false);

		//Returns original image with edges removed
		cv::Mat removeAllLines(const cv::Mat& mat, double rhoRes = 1, int threshold = 30, double thetaDegRes = 0.05, double minLength = 25, double maxLineGap = 10);

		//Returns a mask which masks off all lines found in an image
		cv::Mat removeAllLinesMask(const cv::Mat& mat, double rhoRes = 1, int threshold = 30, double thetaDegRes = 0.05, double minLength = 25, double maxLineGap = 10);

		//Displays the passed image until clicked on
		static void displatMat(cv::Mat displayMat, std::string name = "");

};