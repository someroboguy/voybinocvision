#pragma once
//CPP libaries
#include <mutex>

//Included libraries
#include <opencv2/opencv.hpp>

static const int unusedSearch_2D = 0;
//Local Libraries
#include "StorageObjects.h"
#include "SimpleViewer.h"
#include "StopWatch.h"
#include "RemoveLines.h"


class Search_2D
{
public:
	//	Settings
	//
	double minQuality = floor((double)(2 * 3 * 0.25)); // 2 * pi * q   //?? 0.5
	int cannyLow = 100; //65;
	int cannyHigh = 175; //65;

	//	Public Variables
	//
	Mat original, thresholdMat, opened, difMat, smoothed, edges, houghCircle;
	std::vector<Mat> houghSpaceVec;
	
	//	Constructor
	//
	Search_2D(int imageSizeX, int imageSizeY)
	{
		original = cv::Mat(cv::Size(imageSizeX, imageSizeY), CV_8UC1);
		smoothed = cv::Mat(cv::Size(imageSizeX, imageSizeY), CV_8UC1);
		edges = cv::Mat(cv::Size(imageSizeX, imageSizeY), CV_8UC1);
		houghCircle = Mat(cv::Size(imageSizeX, imageSizeY), CV_8UC1);				
	}

	//	Functions
	//
	std::vector<circle2i> circlePipelineFullImage(cv::Mat& image, const cv::Mat maskMat, 
		std::vector<trackedTarget> currentTargets);
	circle2D circlePipelineROI(cv::Mat& fullImage, circle2D lastCirc, int rMin, int rMax);

	//	Mat functions
	//
	static void drawCircle(Mat& image, cv::Point2f center, double radius, int val);
	static void printMat(Mat image);
	static void inBoundsPoint(Point& iBP, Mat& image);

	//	Alternate Circle Search
	//
	circle2D bullockCircleFit(std::vector<cv::Point2i> circPoints);
	circle2D searchCircleRansac(std::vector<Point2i> allPts);
	circle2D circleSearch(Mat cannyRoiMat);
	double fitError(std::vector<Point2i> checkPts, circle2D fitCirc);


// **********************************************************************************************  //
//    Private				                                                                               //
//																										                                             //
// **********************************************************************************************  //
private:
	//	PipeLine Functions
	//
	Mat getHoughSpace(Mat& edges, int radius);
	circle2i maxPeak(Mat& houghImg);
	circle2D averagedCenter(circle2i center, cv::Mat& houghSpace, int avgBoxSize = 2);

	//	Draw Helpers
	//
	void showDebugWindows(std::vector<circle2D> circles);
	static void drawOffPoints(Mat& image, std::vector<cv::Point2f> offPoints, int colorVal);
	
	//	Tool Functions
	//
	cv::Mat getROI(const cv::Mat& totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax, int scaleFactor = 1);
	void targetMask(Mat& maskMat, circle2i maskTarget);
	void opening(const cv::Mat& originalMat, int numIterations = 2, int elementSize = 2, int thresholdMin = 100, int thresholdMax = 255);
	cv::Mat dilatedDifference(const cv::Mat& mat1, const cv::Mat& mat2, int kernelSize = 2);	
	std::vector<circle2i> inOutCircleCheck(Mat& image, std::vector<circle2i> circles);
	void checkPoints(Mat& image, std::vector<cv::Point2i> diameterPix, int threshold, int& allPix, int& abovePix, int& allIntensity);
	int checkCircle(Mat& image, cv::Point2i center, int radius, int threshold, int& aveIntensity);
};




// **********************************************************************************************  //
//																										                                             //
//    Blob Class		                                                                               //
//																										                                             //
// **********************************************************************************************  //
class Blob
{
public:
	Blob() {}

	Blob(Point2i startPt)
	{
		maxPoint = minPoint = startPt;
		addPoint(startPt);
	}

	std::map<int, std::vector<int>> allPoints;
	Point2i minPoint;
	Point2i maxPoint;

	//	Only center of Max Box, Low Accuracy
	Point2i getCenterPoint()
	{
		int x = maxPoint.x - minPoint.x;
		int y = maxPoint.y - maxPoint.y;
		return Point2i(x, y);
	}

	int getArea()
	{
		int area = 0;

		//	map For Each 
		for (auto& value : allPoints)
		{
			area += value.second.size();
		}

		return area;
	}

	bool isNear(Point2i pt)
	{
		//Point2i centPt = getCenterPoint();
		//int disSq = distanceSquared(pt, centPt);

		int s = 5; // need to be able to grow blob
		if (pt.x > minPoint.x - s && pt.x < maxPoint.x + s && pt.y > minPoint.y - s && pt.y < maxPoint.y + s)
		{
			return true;
		}
		else { return false; }
	}

	void addPoint(Point2i addPt)
	{
		minPoint.x = min(minPoint.x, addPt.x);
		minPoint.y = min(minPoint.y, addPt.y);
		maxPoint.x = max(maxPoint.x, addPt.x);
		maxPoint.y = max(maxPoint.y, addPt.y);

		std::map<int, std::vector<int>>::iterator it = allPoints.find(addPt.x);

		if (it != allPoints.end())
		{
			it->second.push_back(addPt.y);
		}
		else
		{
			allPoints.insert(std::pair<int, std::vector<int>>(addPt.x, std::vector<int>{addPt.y}));
		}
	}

	void maskBlob(Mat& Image, int expand, bool Debug = false)
	{
		for (std::map<int, std::vector<int>>::iterator it = allPoints.begin(); it != allPoints.end(); ++it)
		{
			int x = it->first;
			std::vector<int> Ys = it->second;

			uchar setColor = 0;
			if (Debug) { setColor = 150; }

			// Blob Points
			for (int i = 0; i < Ys.size(); i++)
			{
				// Before Expand Points
				for (int j = -expand; j <= expand; j++)
				{
					for (int k = -expand; k <= expand; k++)
					{
						int xP = min(Image.rows - 1, max(0, x - j));
						int yP = min(Image.cols - 1, max(0, Ys[i] - k));						
						Image.at<uchar>(xP, yP) = setColor;
					}
				}
			}
		}
	}

	void drawContours(Mat& image, Vec3b color)
	{
		for (std::map<int, std::vector<int>>::iterator it = allPoints.begin(); it != allPoints.end(); ++it)
		{
			int x = it->first;
			std::vector<int> Ys = it->second;

			Vec3b heatColor(255, 0, 0);
			Vec3i colorStep(-5, 5, 0);

			int j = 0;
			for (int i = 0; i < Ys.size(); i++)
			{
				heatColor += colorStep;

				image.at<Vec3b>(x, Ys[i]) = heatColor;

				j++;
				if (j > 200) { j = 0; }
				else if (j > 150) { colorStep = Vec3i(-5, 5, 0); }
				else if (j > 100) { colorStep = Vec3i(5, 0, -5); }
				else if (j > 50) { colorStep = Vec3i(0, -5, 5); }
			}
		}
	}

private:
	int distanceSquared(Point2i A, Point2i B) {
		return (A.x - B.x) * (A.x - B.x) + (A.y - B.y) * (A.y - B.y);
	}
};



// **********************************************************************************************  //
//																										                                             //
//    Blob Detect Class		                                                                         //
//																										                                             //
// **********************************************************************************************  //
class BlobDetect
{
public:
	BlobDetect() {
		blobColor = 195;
		filterByArea = true;
		maskInliersTOutliersF = false;
		minArea = 735; // 0.6 * pi * minRadSquared
		maxArea = 20000; // ~2 * pi * maxRadSquared		
		debugPrint = false;
	}

	//  Parameters
	//
	bool debugPrint;

	uchar blobColor;
	bool filterByArea;
	bool maskInliersTOutliersF;
	int minArea, maxArea;

	//
	//
	std::map<int, Blob> allBlobs;

	//  Functions
	//
	void detect(Mat image)
	{
		allBlobs.clear();

		int BYTE_MAX_VAL = 255;

		Mat binaryImg = image.clone();
		//thresholdImage(image, binaryImg, blobColor);
		threshold(image, binaryImg, blobColor, 255, 0);

		int idCount = 2;

		// Loop for Viral Spread
		for (int i = 0; i < binaryImg.rows; i++)
		{
			for (int j = 0; j < binaryImg.cols; j++) {
				if (binaryImg.at<uchar>(i, j) == (uchar)255)
				{
					viralSpread(binaryImg, Point2i(j, i), idCount);
					idCount += 1;
				}
			}
		}

		// Classify Spreads as Blobs
		for (int i = 0; i < binaryImg.rows; i++)
		{
			for (int j = 0; j < binaryImg.cols; j++)
			{
				uchar iVal = binaryImg.at<uchar>(i, j);
				if (iVal != (uchar)0)
				{
					if (allBlobs.count(iVal) == 0)
					{
						allBlobs[iVal] = Blob(Point2i(i, j));
					}
					else
					{
						allBlobs[iVal].addPoint(Point2i(i, j));
					}
				}
			}
		}

		if (filterByArea)
		{
			for (auto it = allBlobs.begin(); it != allBlobs.end(); /* No Increment*/)
			{
				int circArea = it->second.getArea();
				// If inliers remove outlier from list
				if ((!maskInliersTOutliersF) && (circArea > minArea && circArea < maxArea))
				{ allBlobs.erase(it++); }
				else if(maskInliersTOutliersF && (circArea < minArea || circArea > maxArea))
				{ allBlobs.erase(it++); }
				else
				{	++it;	}
			}
		}

		if (debugPrint)
		{
			Mat showImg;
			drawBlobs(image, showImg);
		}
	}

	// Draw
	void drawBlobs(Mat image, Mat blobImg)
	{
		cvtColor(image, blobImg, COLOR_GRAY2BGR);
		//allBlobs[2].drawContours(blobImg, Vec3b(0, 255, 0));

		//	map For Each 
		//	https://stackoverflow.com/questions/6963894/how-to-use-range-based-for-loop-with-stdmap/6963910
		for (auto& value : allBlobs)
		{
			value.second.drawContours(blobImg, Vec3b(0, 255, 0));
		}

		imshow("Blobs", blobImg);
		waitKey(0);
	}

	//void maskBlobs(Mat inImage, Mat outImage, int dialate)
	void maskBlobs(Mat inImage, int dialate)
	{
		//outImage = inImage.clone();

		for (auto& value : allBlobs)
		{
			value.second.maskBlob(inImage, dialate, debugPrint);
		}

		if (debugPrint) 
		{
			imshow("maskBlobs", inImage);
			waitKey(0);
		}
	}

private:

	// viralSpread
	void viralSpread(Mat& binaryImg, Point2i seed, uchar id)
	{
		binaryImg.at<uchar>(seed) = id;

		std::vector<Point2i>* seeds = &std::vector<Point2i>();
		seeds->push_back(seed);

		while (seeds->size() != 0)
		{
			std::vector<Point2i>* newSeeds = new std::vector<Point2i>();
			for (int i = 0; i < seeds->size(); i++)
			{
				std::vector<Point2i> testSeeds
				{ (*seeds)[i] + Point2i(1,0), (*seeds)[i] + Point2i(-1,0), (*seeds)[i] + Point2i(0,1), (*seeds)[i] + Point2i(0,-1) };

				for (int j = 0; j < testSeeds.size(); j++)
				{
					if (testSeeds[j].x < 0 || testSeeds[j].y < 0 || testSeeds[j].x >= binaryImg.cols || testSeeds[j].y >= binaryImg.rows) { continue; }

					if (binaryImg.at<uchar>(testSeeds[j]) == (uchar)255)
					{
						binaryImg.at<uchar>(testSeeds[j]) = id;
						newSeeds->push_back(testSeeds[j]);
					}
				}
			}
			//delete seeds;
			seeds = newSeeds;
		}
	}

	// Threshold Image
	void thresholdImage(Mat& imgIn, Mat& imgOut, uchar threashold)
	{
		for (int i = 0; i < imgIn.rows; ++i)
		{
			uchar* pixel = imgIn.ptr<uchar>(i);  // point to first color in row
			for (int j = 0; j < imgIn.cols; j++, pixel++)
			{
				uchar pix = *pixel;
				if (pix < threashold) { *pixel = (uchar)255; }
				else { *pixel = (uchar)0; }				
			}
		}
		
		if (debugPrint) { imshow("Threshold", imgOut); }
	}
};

