#include "RemoveLines.h"


//Given a matrix
cv::Mat RemoveLines::houghLineTransform(const cv::Mat& mat, double rhoRes, int threshold, double thetaDegRes, double minLength, double maxLineGap, bool debug)
{
	StopWatch timer(1000);
	cv::Mat startMat = mat.clone();
	cv::Mat debugMat;
	if (debug) { debugMat = mat.clone(); }

	cv::Mat onlyLines = cv::Mat(cv::Size(startMat.cols, startMat.rows), CV_8UC1); //Create Blank mat
	onlyLines = Scalar(0); //Set all values to 0

	std::vector<Vec4i> linePoints;

	if (debug) { cv::cvtColor(debugMat, debugMat, COLOR_GRAY2BGR); }

	double rad = thetaDegRes * CV_PI / 180;
	cv::HoughLinesP(startMat, linePoints, rhoRes, rad, threshold, minLength, maxLineGap); //Find lines in image

	for (size_t i = 0; i < linePoints.size(); i++)
	{
		Point pt1, pt2;

		pt1.x = linePoints[i][0];
		pt1.y = linePoints[i][1];
		pt2.x = linePoints[i][2];
		pt2.y = linePoints[i][3];
		if (debug) { line(debugMat, pt1, pt2, Scalar(0, 255, 0), 1, LINE_AA); } //Convert lines to color that can be seen
		line(onlyLines, pt1, pt2, Scalar(255, 255, 255), 1, LINE_AA);
	}

	if (debug)
	{
		displatMat(debugMat, "Lines Mat");
		displatMat(onlyLines, "Only Lines");
	}

	cv::threshold(onlyLines, onlyLines, 200, 255, cv::THRESH_BINARY); //Binarize becuase this can be easily used as mask or for dilation

	return onlyLines;
}

//Returns original image with edges removed
cv::Mat RemoveLines::removeAllLines(const cv::Mat& mat, double rhoRes, int threshold, double thetaDegRes, double minLength, double maxLineGap)
{
	cv::Mat extractedLines = houghLineTransform(mat, rhoRes, threshold, thetaDegRes, minLength, maxLineGap, debugMode);

	//Double Dilate to expand lines
	dilate(extractedLines,1);

	cv::Mat originalWithoutLines;
	cv::subtract(mat, extractedLines, originalWithoutLines);

	if (debugMode)
	{
		displatMat(originalWithoutLines);
	}
	return originalWithoutLines;
}

//Returns a mask which masks off all lines found in an image
cv::Mat RemoveLines::removeAllLinesMask(const cv::Mat& mat, double rhoRes, int threshold, double thetaDegRes, double minLength, double maxLineGap)
{
	cv::Mat extractedLines = houghLineTransform(mat, rhoRes, threshold, thetaDegRes, minLength, maxLineGap, debugMode);

	//Double Dilate to expand lines
	dilate(extractedLines, 1);

	cv::Mat lineMask;
	cv::bitwise_not(extractedLines, lineMask);

	if (debugMode)
	{
		displatMat(lineMask);
	}
	return lineMask;
}

void RemoveLines::displatMat(cv::Mat displayMat, std::string name)
{
	imshow(name, displayMat);
	waitKey(0);
	cv::destroyWindow(name);
}