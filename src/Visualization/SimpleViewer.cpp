#include "SimpleViewer.h"


std::mutex SimpleViewer::imgMutex;
Mat SimpleViewer::globImage = Mat();
int SimpleViewer::lastX = 0;
int SimpleViewer::lastY = 0;
int SimpleViewer::lastIntensity = 0;
int SimpleViewer::lowThreshold = 0;
int SimpleViewer::highThreshold = 0;

void SimpleViewer::imageInspector(std::string filePath)
{
	imageInspector(imread(filePath, CV_LOAD_IMAGE_GRAYSCALE));
}

void SimpleViewer::imageInspector(Mat inputImage)
{
	std::cout << "Press x in window to exit viewer" << std::endl;
	globImage = inputImage.clone();
	lastX = 0;
	lastY = 0;
	lastIntensity = 0;

	namedWindow("Display Image", WINDOW_AUTOSIZE);
	setMouseCallback("Display Image", onMouse, 0);

	bool continueDisplay = true;
	//while (continueDisplay )
	{
		cv::Mat image = globImage.clone();

		rectangle(image, Point(0, 0), Point(400, 29), USHRT_MAX, -1);
		std::string mouseClick = "pt: " + std::to_string(lastX) + "," + std::to_string(lastY) + "; intensity: " + std::to_string(lastIntensity);
		cv::putText(image, mouseClick, cv::Point(0, 15), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, 0);

		imshow("Display Image", image);

		//ascii table
		int input = waitKey(2000);//10);
		switch (input)
		{
		case((int)'x'):
			continueDisplay = false;
			break;
		}
	}
}

void SimpleViewer::edgeDetectionTesting(std::string filePath)
{
	edgeDetectionTesting(imread(filePath, CV_LOAD_IMAGE_GRAYSCALE));
}

void SimpleViewer::edgeDetectionTesting(Mat inputImage)
{
	globImage = inputImage;

	namedWindow("Canny Filter Testing", WINDOW_AUTOSIZE);
	createTrackbar("Min:", "Canny Filter Testing", &lowThreshold, 250, CannyThreshold);
	createTrackbar("Max:", "Canny Filter Testing", &highThreshold, 250, CannyThreshold);
	SimpleViewer::CannyThreshold(0, 0);
	//imshow("Edge Filtered", edges);]
	waitKey(0);
}