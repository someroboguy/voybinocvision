#pragma once
//CPP libaries
#include <mutex>

//Included libraries
#include <opencv2/opencv.hpp>

static const int unusedSimpleViewer = 0;
//Local Libraries

using namespace cv;

class SimpleViewer
{
public:
	static void imageInspector(std::string filePath);
	static void imageInspector(Mat inputImage);
	static void edgeDetectionTesting(std::string filePath);
	static void edgeDetectionTesting(Mat inputImage);

private:
	static std::mutex imgMutex;
	static Mat globImage;
	static int lastX;
	static int lastY;
	static int lastIntensity;

	static void onMouse(int event,int x, int y, int, void*)
	{
		if (event != CV_EVENT_LBUTTONDOWN)
			return;

		lastX = x;
		lastY = y;
		imgMutex.lock();
		lastIntensity = globImage.at<cv::uint8_t>(y, x);
		imgMutex.unlock();
	}


	static int lowThreshold;
	static int highThreshold;
	static void CannyThreshold(int, void*)
	{
		Mat blurred;
		Mat edges;
		blur(globImage, blurred, Size(3, 3));
		//GaussianBlur(globImage, blurred, Size(3, 3), 1, 1);
		Canny(blurred, edges, lowThreshold, highThreshold, 3,true);
		imshow("Canny Filter Testing", edges);
	}
};