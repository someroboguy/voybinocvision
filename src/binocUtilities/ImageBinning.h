#pragma once
//CPP libaries
#include <mutex>
#include <string>
#include <iostream>
#include <fstream>

#include <opencv2/opencv.hpp>


class ImageBinning
{
	private:
		std::vector<int> binnedImage;
		int binSize;
		int binWidth;
		int numBins;

		//Bin filters used for cutting off data that you dont want displayed
		int binFilterHigh;
		int binFilterLow;

		//Bubble sort to sort indicies by their peak size
		std::vector<int> indexSort(std::vector<int> peakIndicies, std::vector<int> _binnedImage)
		{
			for (int i = 0; i < peakIndicies.size(); i++)
			{
				for (int j = 1; j < peakIndicies.size() - i; j++)
				{
					if (_binnedImage[peakIndicies[j]] < _binnedImage[peakIndicies[j - 1]])
					{
						int tempIndex = peakIndicies[j];
						peakIndicies[j] = peakIndicies[j - 1];
						peakIndicies[j - 1] = tempIndex;
					}
				}
			}

			return peakIndicies;
		}

		double getStandardDeviation(int val, int mean, int numSamples)
		{
			double stdev = 0;
			double squaredDifference = std::pow((val - mean), 2);
			stdev = sqrt(squaredDifference / numSamples);
			return stdev;
		}

		//Could use some work to better find bottom. Potentially add a tolerance or a confidence level
		int findFirstLowestBinIndex(std::vector<int> _binnedImage, int startIndex = 1)
		{
			int delta; 
			int index = 0;
			bool startedDrop = false;
			for (int i = startIndex+1; i < _binnedImage.size(); i++)
			{
				delta = _binnedImage[i] - _binnedImage[i-1];

				if (delta < 0)
				{
					if (!startedDrop) { startedDrop = true; }
					index = i;
				}

				if (delta > 0 && startedDrop)
				{
					break;
				}
			}

			return index;
		}
	
		

		int findFirstNonZeroIndex(std::vector<int> _binnedImage)
		{
			int startIndex = 0;
			//Find first index with a value due to data trimming.
			for (int i = 0; i < _binnedImage.size(); i++)
			{
				if (_binnedImage[i] == 0) { continue; }
				else { startIndex = i; break; }
			}
			return startIndex;
		}


	public:

		ImageBinning(int _binSize = 1, int _binWidth = 255)
		{
			binSize = _binSize;
			binWidth = _binWidth;
			numBins = binWidth / binSize;
			binFilterHigh = binWidth;
			binFilterLow = 0;
		}

		void resizeBin(int _newBinSize, int _newBinWidth = 255);


		int getNumBins()
		{
			return numBins;
		}
		
		int getBinSize()
		{
			return binSize;
		}

		std::vector<int> getBinnedImage() { return binnedImage; }
		void updateBinFilters(int _binFilterHigh, int _binFilterLow);
		 
		//Bins a passed CV::Mat
		//	resetBin: Will reset all values in the bin to 0 before binning the current image
		//	useBinFilters: Will use the current set bin filters to cut out unwanted data 
		std::vector<int> binImage(const cv::Mat& mat, bool resetBin = true, bool useBinFilters = true);
		
		//Goes through the current binned image and applies an averaging over a window filter size
		//Performs this operation inplace
		std::vector<int> smoothBins(int windowFilterSize, bool inPlace = true);

		//Finds the largest change in binsize from one bin to another given a filtersize
		//Returns the bin value. Maybe return index
		int findLargestChangeIndex(int windowFilterSize = 1);

		//Tolerance is percentage above the mean that the peak has to be to be registered as a peak
		std::vector<int> findPeakBin(int numPeaks, std::vector<int> _binnedImage, int tolerance = 0.6, int movingAverageSize = 5, int minVal = 50)
		{
			//Start at the lowest index.
			int startingIndex = findFirstLowestBinIndex(_binnedImage);
			std::vector<int> peakIndicies;
			for (int i = startingIndex + movingAverageSize; i < _binnedImage.size(); i++)
			{
				int windowMean = 0;	//get moving average
				for (int j = movingAverageSize; j > 0; j--)
				{
					windowMean += _binnedImage[i - j];
				}
				windowMean /= movingAverageSize;

				//double stdev = getStandardDeviation(_binnedImage[i], windowMean, movingAverageSize);

				if (_binnedImage[i] > (windowMean*(1+tolerance))) //check if 
				{
					if (_binnedImage[i] > minVal)
					{
						peakIndicies.push_back(i);
					}
				}
			}

			return indexSort(peakIndicies, _binnedImage);
		}



		//TODO: Can replace with own version if you have FileIO in project
		//      Left this as standard IO to keep generic
		void writeBinToFile(std::string filePath);
};