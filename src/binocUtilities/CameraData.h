#pragma once
//CPP libaries
#include <mutex>
#include <string>
#include <chrono>
#include <iostream>
//#include <sstream>
//#include <fstream>
#include "FileIO.h"

#include <direct.h>

#include "ApiController.h"
#include <opencv2/opencv.hpp>
#include "Search_2D.h"
//Included libraries

class CameraData
{
public:
	// Add fixDistorion, Clip Image, Light Correct here as a bool. (or enum!)
	CameraData(std::string _camId, int _playbackSize = 100);

	void startCam()
	{
		apiControllerPtr->StartContinuousImageAcquisition(camID);
		std::cout << "Cam Shooting" << endl;
		std::string xmlFile = "../../resources/CameraConfigs/";
		xmlFile += camID;
		std::string openCV_Config = xmlFile + "/openCV_Config.xml";

		ifstream f(openCV_Config);
		if (f.good())
		{
			cv::FileStorage opencv_file;
			opencv_file = cv::FileStorage(openCV_Config, cv::FileStorage::READ);
			opencv_file["Camera_Matrix"] >> camMatrix;
			opencv_file["Distortion_Coefficients"] >> camDistCoeff;
			opencv_file.release();
		}
	}

	~CameraData()
	{
		// look into if deconstructor 
		//apiControllerPtr->StopContinuousImageAcquisition();
		//apiControllerPtr->ShutDown();
	}
	CameraData& operator=(const CameraData& obj);
	void closeCam();

	//	Test
	cv::Mat openTest(cv::Mat originalMat, bool show = true);
	
	//	Masks
	void resetMask(int color);
	Mat getMaskMat();
	void setPolyMask(vector<Point> points, bool outside, uchar color);

	// Targets
	std::vector<trackedTarget> getCamTargets();
	trackedTarget getCamTarget(int targetUID);
	void addCamTarget(trackedTarget trackTarget);
	void updateCamTarget(trackedTarget trackTarget);
	void removeCamTarget(int targetUID);

	// Images
	Mat getLatestImage(bool fixDistortion = true);
	void generateOpenCVDistortionMaps(Mat image);
	Mat roiMat(cv::Mat totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax);
	void lightCorrect(Mat& image, vector<double>& lightMap);
	vector<double> calcLight(Mat image);

	//	Play Back
	//
	void updateLatestImage(cv::Mat& latestImage, cv::Mat& latestMask = cv::Mat(cv::Size(0, 0), CV_8UC1));
	void startPlayback();
	void stopPlayback();

	void saveMat(std::string nameString);
	static void saveMat(cv::Mat saveMat, std::string nameString);
	static void saveMat(const std::vector<cv::Mat>& saveMats, std::string folderName, std::string nameString = "");
	void savePlayback(std::string saveName, double percentToSave = 1);
	cv::Mat loadMat(std::string nameString, bool displayMat);
	std::vector<cv::Mat> loadMultiMat(std::string folderPath);
	cv::Mat getLatestPlayBackMat() { return playbackImages.at(playbackIndex); }
	static void displatMat(cv::Mat displayMat, std::string name = "");

//
//	Variables
//
	AVT::VmbAPI::Stream::ApiController* apiControllerPtr;
	std::string camID;

	bool playbackStore = true;
private:
	std::mutex playbackImageMutex;
	std::vector<cv::Mat> playbackImages;
	std::vector<cv::Mat> playbackMasks;
	int playbackSize;
	int playbackIndex = 0;
	bool playbackVideoFlag = false;
	bool playbackStoreFlag = false;
	std::thread playbackThread;

	int picID = -1;	
	cv::Mat currentImage, camMask, camMatrix, camDistCoeff;
	std::mutex currentImageMutex, cameraMaskMutex, camTargetsMutex;
	std::vector<trackedTarget> camTargets;

	Mat openCVDistortionMap1, openCVDistortionMap2;
	vector<double> camLightMap;
	bool fixLight = true;
	void playbackVideo();


	static bool createFolder(std::string folderName)
	{
		std::string filepath = "../../resources/images/" + folderName;
		const char* cFilepath = filepath.c_str();
		if (mkdir(cFilepath) != 0) { std::cout << "Unable to create directory;\n"; return false; };
		return true;
	}
};