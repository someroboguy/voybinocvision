#pragma once

#ifndef FILEIO_H
#define FILEIO_H


#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>

using namespace std;

//This class operates relative to resource path
//TODO: bug-> if you attempt to save or load from a folder that doesnt exist, it will crash
class FileIO
{
public:
	static bool saveSingleItem(string fileName, string dataItem, bool append);

	static string loadSingleItem(string fileName);

	static bool saveMultiItem(string fileName, vector<string> dataItems, bool append);

	static vector<string> loadMultiItem(string fileName);


private:
	static string resourcePath;
	static bool dirExists(const std::string& dirName_in);
	static std::string dataLocationDir;
	static bool localDirSet;

	static void updateLocalDir()
	{
		localDirSet = true;
		int breakoutCounter = 0;
		while (!dirExists(resourcePath))
		{
			breakoutCounter++;
			resourcePath = "../" + resourcePath;
			
			if (breakoutCounter > 5) { std::cout << "Failed to locate local directories, all local file operations will fail..." << std::endl; return; }
		}
	}
};

#endif