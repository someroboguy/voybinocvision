#include "CameraData.h"

CameraData::CameraData(std::string _camId, int _playbackSize)
{
	camID = _camId;
	AVT::VmbAPI::Stream::ApiController* a = new AVT::VmbAPI::Stream::ApiController();
	apiControllerPtr = a;
	VmbErrorType err = apiControllerPtr->StartUp();
	std::cout << "Cam Started" << endl;
	
	playbackSize = _playbackSize;
	for (int i = 0; i < playbackSize; i++)
	{
		playbackImages.push_back(cv::Mat(cv::Size(800, 600), CV_8UC1));
		playbackMasks.push_back(cv::Mat(cv::Size(0, 0), CV_8UC1));
	}
}

// override default copy constructor without mutex copying
CameraData& CameraData::operator=(const CameraData& obj)
{
	//prop = obj.prop for all but mutex	
	playbackImages = obj.playbackImages;
	playbackMasks = obj.playbackMasks;
	playbackSize = obj.playbackSize;
	playbackVideoFlag = false;	

	return *this;
}

void CameraData::closeCam()
{
	apiControllerPtr->StopContinuousImageAcquisition();
	apiControllerPtr->ShutDown();
	apiControllerPtr->~ApiController();
	//delete apiControllerPtr;
}

//
//
//
//

cv::Mat CameraData::openTest(cv::Mat originalMat, bool show)
{
	cv::Mat openMat;

	cv::Mat thresholdMat;
	threshold(originalMat, thresholdMat, 100, 255, CV_THRESH_BINARY_INV);

	int erosion_size = 2;

	Mat element = cv::getStructuringElement(cv::MORPH_CROSS,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));


	cv::morphologyEx(thresholdMat, openMat, cv::MORPH_OPEN, element, cv::Point(-1, -1), 2);

	cv::Mat difMat;
	cv::absdiff(thresholdMat, openMat, difMat);

	cv::dilate(difMat, difMat, element);

	if (show)
	{

		imshow("Threshold Image", thresholdMat);
		waitKey(0);
		cv::destroyWindow("Threshold Image");

		imshow("Opened Image", openMat);
		waitKey(0);
		cv::destroyWindow("Opened Image");

		imshow("Dif Image", difMat);
		waitKey(0);
		cv::destroyWindow("Dif Image");
	}

	return openMat;
}


//
//	Masks
//
void CameraData::resetMask(int color)
{
	cameraMaskMutex.lock();
	if (color == -1)
	{
		camMask = Mat();
	}
	else
	{
		camMask = cv::Mat(cv::Size(currentImage.rows, currentImage.cols), CV_8UC1, Scalar(color));
	}
	cameraMaskMutex.unlock();
}

Mat CameraData::getMaskMat()
{
	Mat retMat;
	cameraMaskMutex.lock();
	retMat = camMask.clone();
	cameraMaskMutex.unlock();
	return retMat;
}

void CameraData::setPolyMask(vector<Point> points, bool outside, uchar color)
{
	cameraMaskMutex.lock();
	for each (Point tPt in points) { Search_2D::inBoundsPoint(tPt, camMask); }
	if (outside)
	{
		fillConvexPoly(camMask, points, color);
	}
	else // False
	{
		fillPoly(camMask, points, color);
	}
	cameraMaskMutex.unlock();
}

//
//	Targets
//
std::vector<trackedTarget> CameraData::getCamTargets()
{
	std::vector<trackedTarget> retTargets;
	camTargetsMutex.lock();
	retTargets = camTargets;
	camTargetsMutex.unlock();
	return retTargets;
}

trackedTarget CameraData::getCamTarget(int targetUID)
{
	for (int i = 0; i < camTargets.size(); i++)
	{
		if (camTargets[i].getUID() == targetUID) { return camTargets[i]; }
	}
	return trackedTarget();
}

void CameraData::addCamTarget(trackedTarget trackTarget)
{
	camTargetsMutex.lock();
	camTargets.push_back(trackTarget);
	camTargetsMutex.unlock();
}

void CameraData::updateCamTarget(trackedTarget trackTarget)
{
	for (int i = 0; i < camTargets.size(); i++)
	{
		if (camTargets[i].getUID() == trackTarget.getUID())
		{
			camTargetsMutex.lock();
			camTargets[i] = trackTarget;
			camTargetsMutex.unlock();
			return;
		}
	}
}

void CameraData::removeCamTarget(int targetUID)
{
	for (int i = 0; i < camTargets.size(); i++)
	{
		if (camTargets[i].getUID() == targetUID)
		{
			camTargetsMutex.lock();
			camTargets.erase(camTargets.begin() + i);
			camTargetsMutex.unlock();
			return;
		}
	}
}

//
//	Images
//
Mat CameraData::getLatestImage(bool fixDistortion)
{
	int currentPicId = apiControllerPtr->getImageID();
	
	if (picID == currentPicId)
	{
		Mat lImg;
		currentImageMutex.lock();
		lImg = currentImage.clone();
		currentImageMutex.unlock();
		return lImg;
	}
	else
	{
		Mat image = apiControllerPtr->getLastImage();
		if (image.empty() || image.rows != 800 || image.cols != 600) { return image; }

		picID = currentPicId;

		// Adds pic to memory store buffer			
		if (playbackStore)
		{
			updateLatestImage(image, getMaskMat());
		}

		// Camera Distortion Fix
		if (!camDistCoeff.empty() && fixDistortion)
		{
			Mat rview;

			if (openCVDistortionMap1.empty()) { generateOpenCVDistortionMaps(image); }

			remap(image, rview, openCVDistortionMap1, openCVDistortionMap2, INTER_LINEAR);
			image = roiMat(rview, Point2i(10, 10), Point2i(780, 580));
		}

		if (fixLight)
		{
			if (camLightMap.size() < 2) { camLightMap = calcLight(image); }
			lightCorrect(image, camLightMap);
		}

		currentImageMutex.lock();
		currentImage = image.clone();
		currentImageMutex.unlock();
		return image;
	}
}

void CameraData::generateOpenCVDistortionMaps(Mat image)
{
	Size imageSize;
	imageSize = image.size();
	initUndistortRectifyMap(camMatrix, camDistCoeff, Mat(),
		getOptimalNewCameraMatrix(camMatrix, camDistCoeff, imageSize, 1, imageSize, 0),
		imageSize, CV_16SC2, openCVDistortionMap1, openCVDistortionMap2);
}

Mat CameraData::roiMat(cv::Mat totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax)
{
	//bound to protect from OB of totalMatrix
	ptMin.x = std::max(0, ptMin.x);
	ptMin.y = std::max(0, ptMin.y);
	ptMax.x = std::min(totalMat.cols - 1, ptMax.x);
	ptMax.y = std::min(totalMat.rows - 1, ptMax.y);

	/*if (ptMax.x < 0 || ptMax.y < 0) { ptMax.x = 0; ptMax.y = 0; std::cout << "Error\n"; }*/

	Mat miniMat = Mat(cv::Size(ptMax.x - ptMin.x + 1, ptMax.y - ptMin.y + 1), CV_8UC1);
	for (int i = ptMin.x; i <= ptMax.x; i++)
	{
		for (int ii = ptMin.y; ii <= ptMax.y; ii++)
		{
			miniMat.at<cv::uint8_t>(ii - ptMin.y, i - ptMin.x) = totalMat.at<cv::uint8_t>(ii, i);
		}
	}
	return miniMat;
}

void CameraData::lightCorrect(Mat& image, vector<double>& lightMap)
{
	//uchar* pI = image.data;
	vector<double>::iterator pV = lightMap.begin();

	for (int i = 0; i < image.rows; ++i)
	{
		uchar* pixel = image.ptr<uchar>(i);  // point to first color in row
		for (int j = 0; j < image.cols; j++, pixel++, pV++)
		{
			*pixel = min((int)(*pixel * *pV), 255);
		}
	}


	//for (int i = 0; i < image.cols * image.rows; i++, pI++, pV++)
	//{
	//	pI[0] = min((int)(pV[0] * pI[0]), 255);
	//	//ToDo: verify
	//	//*pI = min((int)(*pV * *pI), 255);
	//}
}

vector<double> CameraData::calcLight(Mat image)
{
	double gain = 0.0002;
	double start = 0.90;
	int iC = (image.rows / 2);
	int jC = (image.cols / 2);

	vector<double> lightMap(image.rows * image.cols);
	int k = 0;

	for (int i = 0; i < image.rows; ++i)
	{
		uchar* pixel = image.ptr<uchar>(i);  // point to first color in row
		for (int j = 0; j < image.cols; j++, pixel++)
		{

			//for (int i = 0; i < image.rows; i++)
			//{
			//	for (int j = 0; j < image.cols; j++)
			//	{
			double distance = sqrt(pow((iC - i) * 6, 2) * 1 + pow((jC - j) * 8, 2)); // 800X600 so * opposite
			double val = ((gain * distance) + start);
			lightMap[k] = val;
			k++;
		}
	}
	return lightMap;
}

//
//
//
//
void CameraData::playbackVideo()
{
	playbackImageMutex.lock();
	std::vector<cv::Mat> currentPlaybackImages = playbackImages;
	playbackImageMutex.unlock();

	while (playbackVideoFlag)
	{
		for (int i = 0; i < currentPlaybackImages.size(); i++)
		{
			cv::Mat displayImage = currentPlaybackImages[i];
			cv::imshow("Playback", displayImage);
			cv::waitKey(5);
			std::this_thread::sleep_for(std::chrono::milliseconds(15));
		}
	}
	cv::destroyWindow("Playback");
}

void CameraData::updateLatestImage(cv::Mat& latestImage, cv::Mat& latestMask)
{
	if (playbackStoreFlag)
	{
		playbackImageMutex.lock();
		playbackImages.at(playbackIndex) = latestImage;
		//playbackMasks.at(playbackIndex) = latestMask;
		playbackImageMutex.unlock();

		playbackIndex++;
		if (playbackIndex == playbackSize)
		{
			playbackIndex = 0;
		}
	}
}

void CameraData::startPlayback()
{
	if (playbackVideoFlag == true) { std::cout << "Already in playback\n"; return; }
	playbackVideoFlag = true;
	playbackThread = std::thread([this] {playbackVideo(); });
}

void CameraData::stopPlayback()
{
	playbackVideoFlag = false;
	if (playbackThread.joinable()) { playbackThread.join(); }
}

/// Used to save the current cameraImage
void CameraData::saveMat(std::string nameString)
{
	if (apiControllerPtr == NULL) { std::cout << "Camera not found\n"; return; }
	Mat displayImage = getLatestImage();

	cv::imwrite("../../resources/images/" + nameString + ".bmp", displayImage);

	imshow("Saved Image", displayImage);
	waitKey(0);
	cv::destroyWindow("Saved Image");
}

//Used to save an image passed to the fn
void CameraData::saveMat(cv::Mat saveMat, std::string nameString)
{
	Mat displayImage = saveMat;

	cv::imwrite("../../resources/images/" + nameString + ".bmp", displayImage);
}

///<Summary>Saves a matrix of images</Summary>
void CameraData::saveMat(const std::vector<cv::Mat>& saveMats, std::string folderName, std::string nameString)
{
	createFolder(folderName); //Could have breakout if it cant create folder but folder may be present already
	
	for (int i = 0; i < saveMats.size(); i++)
	{
		Mat displayImage = saveMats[i];
		cv::imwrite("../../resources/images/" + folderName + "/" + nameString + std::to_string(i) + ".bmp", displayImage);
	}
}

//TODO: Save a vector of Mats?
//saves a percentage of images
void CameraData::savePlayback(std::string saveFolder, double percentToSave)
{
	playbackStoreFlag = false;
	int numSamples = playbackImages.size();
	int numToSave = numSamples * percentToSave;
	int currentIndex = playbackIndex;


	if (!createFolder(saveFolder)) { return; }


	//Configfile
	std::string saveData = std::to_string(numToSave);
	std::ofstream outputFile("../../resources/images/" + saveFolder + "/config.txt");
	outputFile << saveData;
	outputFile.close();

	int indexNumber = 0; //used to differentiate images

	while(indexNumber < numToSave)
	{
		currentIndex--;
		if (currentIndex == -1) { currentIndex = playbackImages.size() - 1; }

		playbackImageMutex.lock();
		cv::Mat currentImage = playbackImages[currentIndex];
		cv::Mat currentMask = playbackMasks[currentIndex];
		playbackImageMutex.unlock();

		if ((currentMask.rows == currentImage.rows) && (currentMask.cols == currentImage.cols))
		{
			for (int i = 0; i < currentMask.cols; i++)
			{
				for (int j = 0; j < currentMask.rows; j++)
				{
					currentImage.at<cv::uint8_t>(j, i) *= currentMask.at<uint8_t>(j, i);
				}
			}
		}
		
		cv::imwrite("../../resources/images/" + saveFolder + "/" + std::to_string(indexNumber) + ".bmp", currentImage);
		
		indexNumber++;
	}

	playbackStoreFlag = true;
}

cv::Mat CameraData::loadMat(std::string nameString, bool displayMat)
{
	Mat displayImage = cv::imread("../../resources/images/" + nameString + ".bmp", CV_8UC1);

	if (displayMat)
	{
		imshow("Loaded Image", displayImage);
		waitKey(0);
		cv::destroyWindow("Loaded Image");
	}

	return displayImage;
}

std::vector<cv::Mat> CameraData::loadMultiMat(std::string folderName)
{
	std::vector<cv::Mat> readMats;
	try
	{
		std::ifstream readFile("../../resources/images/" + folderName + "/config.txt");
		
		std::string numSamplesString;
		std::getline(readFile, numSamplesString);
		int numSamples = std::stoi(numSamplesString);

		std::string nameString = "";
		std::getline(readFile, nameString);
		for (int i = 0; i < numSamples; i++)
		{
			cv::Mat currentMat = loadMat(folderName + "/" + nameString + std::to_string(i), false);
			readMats.push_back(currentMat);
		}
	}
	catch (...)
	{
		std::cout << "Error in reading.\n";
	}
	return readMats;
}

void CameraData::displatMat(cv::Mat displayMat, std::string name)
{
	imshow(name, displayMat);
	waitKey(0);
	cv::destroyWindow(name);
}