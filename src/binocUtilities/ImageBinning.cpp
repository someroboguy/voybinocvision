#include "ImageBinning.h"

void ImageBinning::resizeBin(int _newBinSize, int _newBinWidth)
{
	binSize = _newBinSize;
	binWidth = _newBinWidth;
	numBins = binWidth / binSize;
	binnedImage = std::vector<int>(numBins, 0);
}

void ImageBinning::updateBinFilters(int _binFilterHigh, int _binFilterLow)
{
	binFilterHigh = _binFilterHigh;
	binFilterLow = _binFilterLow;
}

std::vector<int> ImageBinning::binImage(const cv::Mat& mat, bool resetBin, bool useBinFilters)
{
	//reset
	if (resetBin) { binnedImage = std::vector<int>(numBins, 0); }

	for (int i = 0; i < mat.cols; i++)
	{
		for (int j = 0; j < mat.rows; j++)
		{
			int pixelVal = mat.at<cv::uint8_t>(j, i);
			int bin = pixelVal / binSize;

			if (bin >= numBins) 
			{
				bin = numBins - 1;
			}
			

			if (pixelVal < binFilterLow || pixelVal > binFilterHigh)
			{
				binnedImage.at(bin) = 0;
			}
			else
			{
				binnedImage.at(bin) = binnedImage.at(bin) + 1;
			}
		}
	}

	if (useBinFilters) //Set all 0 values before first nonZero
	{
		int firstNonZeroBinIndex = findFirstNonZeroIndex(binnedImage);
		int firstNonZeroBinVal = binnedImage[firstNonZeroBinIndex];

		for (int i = 0; i < firstNonZeroBinIndex; i++)
		{
			binnedImage[i] = firstNonZeroBinVal;
		}
	}

	return binnedImage;
}


std::vector<int> ImageBinning::smoothBins(int windowFilterSize, bool inPlace)
{

	std::vector<int> smoothedBin(binnedImage.size(), 0);

	int minWinFilterSize = windowFilterSize / 2;
	for (int i = minWinFilterSize; i < binnedImage.size() - minWinFilterSize; i++)
	{
		double meanTotal = 0;
		for (int j = i - minWinFilterSize; j <= i + minWinFilterSize; j++)
		{
			meanTotal += binnedImage.at(j);
		}

		smoothedBin[i] = meanTotal / windowFilterSize;
	}

	if (inPlace)
	{
		binnedImage = smoothedBin;
	}
	
	return binnedImage;
}

//Change to return the bin index.
int ImageBinning::findLargestChangeIndex(int windowFilterSize)
{
	std::vector<int> smoothedBin = smoothBins(windowFilterSize, false);

	int startIndex = findFirstNonZeroIndex(smoothedBin);

	int largestDeltaBinIndex = 0;
	int largestDelta = 0;

	for (int i = startIndex + 1; i < smoothedBin.size(); i++)
	{
		double delta = abs(smoothedBin[i] - smoothedBin[i - 1]);
		if (delta > largestDelta) 
		{ 
			largestDeltaBinIndex = i;
			largestDelta = delta;
		
		}
	}

	return largestDeltaBinIndex;
}


//TODO: Can replace with own version if you have FileIO in project
//      Left this as standard IO to keep generic
void ImageBinning::writeBinToFile(std::string filePath)
{
	std::string writeString = "";
	int binNum = 0;

	for (int i = 0; i < binnedImage.size(); i++)
	{
		writeString += std::to_string(binNum) + "," + std::to_string(binnedImage.at(i)) + "\n";
		binNum += binSize;
	}

	std::ofstream writeStream;
	writeStream.open(filePath);
	writeStream << writeString;
	writeStream.close();
}