#pragma once
//CPP libaries
#include <mutex>
#include <string>
#include <chrono>
#include <iostream>

//Included libraries

static const int unusedStorageObjects = 0;
//Local Libraries

//Code Top
//using namespace std::chrono;
class circle2i {
public:
	int x;
	int y;
	int rad;
	uint8_t mag;

	circle2i()
	{
		x = 0; y = 0; rad = 0; mag = 0;
	}
};


class circle2D {
public:
	double x;
	double y;
	double rad;
	double mag;

	circle2D()
	{
		x = 0; y = 0; rad = 0; mag = 0;
	}

	circle2D(double _x, double _y, double _rad, double _mag)
	{
		x = _x; y = _y; rad = _rad; mag = _mag;
	}

	circle2D(circle2i iC)
	{
		x = iC.x;
		y = iC.y;
		rad = iC.rad;
		mag = (double)iC.mag;
	}

	// https://stackoverflow.com/questions/4824278/c-defining-a-type-cast
	operator circle2i() 
	{
		circle2i iC;
		iC.x = x;
		iC.y = y;
		iC.rad = rad;
		iC.mag = mag;
		return iC;
	}

	std::string print()
	{
		std::string output = "";
		output += "x: " + std::to_string(x);
		output += ", y: " + std::to_string(y);
		output += ", rad: " + std::to_string(rad);
		output += ", mag: " + std::to_string(mag);
		return output;
	}

	std::string printCSV()
	{
		std::string output = "";

		output += std::to_string(x) + ",";
		output += std::to_string(y) + ",";
		output += std::to_string(rad) + ",";
		output += std::to_string(mag);
		return output;
	}
	static double distance(circle2D circ1, circle2D circ2)
	{
		return std::sqrt((circ1.x - circ2.x)*(circ1.x - circ2.x) + (circ1.y - circ2.y)*(circ1.y - circ2.y));
	}

	circle2D& operator += (circle2D const& obj) 
	{	
		x += obj.x;
		y += obj.y;
		rad += obj.rad;
		mag += obj.mag;
		return *this;
	}

	circle2D& operator /= (double const& num)
	{
		x /= num;
		y /= num;
		rad /= num;
		mag /= num;
		return *this;
	}
};

class trackedTarget {
public:
	bool stopTracking = false;
	circle2D lastLocation;

	trackedTarget() 
	{ stopTracking = true; }

	trackedTarget(circle2D targetLocation)
	{
		lastLocation = targetLocation;
		//if (std::isnan(targetLocation.x)) { std::cout << "Error!!!!!\n"; }
		lastFoundTime = std::chrono::steady_clock::now();
		targetStartTime = std::chrono::steady_clock::now();
		trackingActive = true;
		uID = uidGenerator++;
	}
	
	int getUID() { return uID; }

	trackedTarget& operator=(const trackedTarget& obj)
	{
		lastLocation = obj.lastLocation;
		trackingActive = obj.trackingActive;
		lastFoundTime = obj.lastFoundTime;
		targetStartTime = obj.targetStartTime;
		trackedFrequency = obj.trackedFrequency;
		uID = obj.uID;
		uidGenerator = obj.uidGenerator;

		return *this;
	}

	trackedTarget(const trackedTarget& obj)
	{
		lastLocation = obj.lastLocation;
		trackingActive = obj.trackingActive;
		lastFoundTime = obj.lastFoundTime;
		targetStartTime = obj.targetStartTime;
		trackedFrequency = obj.trackedFrequency;
		uID = obj.uID;
		uidGenerator = obj.uidGenerator;
	}

	void updateTrackedTarget(circle2D updatedCircle)
	{
		lastLocation = updatedCircle;
		trackingActive = true;
		trackedFrequency = 1000000.0 / std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - lastFoundTime).count();
		lastFoundTime = std::chrono::steady_clock::now();
	}

	void updateTrackedTarget()
	{
		trackingActive = false;
		trackedFrequency = 0.00;
		//1000000.0 / std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - lastFoundTime).count();
	}

	long microsSinceLastFound()
	{
		return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - lastFoundTime).count();
	}

	circle2D getLastLocation()
	{
		//if (lastLocation.x < 0) { std::cout << "major error\n"; }
		circle2D temp = circle2D(lastLocation.x, lastLocation.y, lastLocation.rad, lastLocation.mag);
		return lastLocation;
	}
	bool getTrackingActive() { return trackingActive; }
	double getTrackedFrequency() { return trackedFrequency; }

	std::string to_serial()
	{
		std::string outMsg = "";
		outMsg += std::to_string(uID) + ",";
		outMsg += std::to_string(lastLocation.x) + ",";
		outMsg += std::to_string(lastLocation.y) + ",";
		outMsg += std::to_string(lastLocation.rad) + ",";
		outMsg += std::to_string(lastLocation.mag) + ",";
		outMsg += std::to_string(trackingActive) + ",";
		outMsg += std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(lastFoundTime-targetStartTime).count());
		return outMsg;
	}
	
private:
	//mutable std::mutex accessMut;
	//circle2D lastLocation;
	bool trackingActive = false;
	std::chrono::time_point<std::chrono::steady_clock> lastFoundTime;
	std::chrono::time_point<std::chrono::steady_clock> targetStartTime;
	double trackedFrequency = 0;
	int uID = 0;
	static int uidGenerator;
};



