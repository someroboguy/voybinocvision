#include "FileIO.h"

string FileIO::resourcePath = "../../resources/images/";
bool FileIO::localDirSet = false;


bool FileIO::saveSingleItem(string fileName, string dataItem, bool append)
{
	if (!localDirSet) { updateLocalDir(); }
	string fullFilePath = resourcePath + fileName;
	ofstream file;

	if (append) {
		file.open(fullFilePath, std::ios_base::app);
	}
	else {
		file.open(fullFilePath);
	}

	if (file.is_open())
	{
		file << dataItem << endl;
		file.close();
		return true;
	}
	else { return false; }
}

string FileIO::loadSingleItem(string fileName)
{
	if (!localDirSet) { updateLocalDir(); }
	string item = "";
	string fullFilePath = resourcePath + fileName;
	ifstream file(fullFilePath);
	if (file.is_open())
	{
		if (!getline(file, item)) { return ""; }
	}
	return item;
}

bool FileIO::saveMultiItem(string fileName, vector<string> dataItems, bool append)
{
	if (!localDirSet) { updateLocalDir(); }
	string fullFilePath = resourcePath + fileName;
	ofstream file;

	if (append) {
		file.open(fullFilePath, std::ios_base::app);
	}
	else {
		file.open(fullFilePath);
	}

	if (file.is_open())
	{
		for (int i = 0; i < dataItems.size(); i++)
		{
			file << dataItems[i] << endl;
		}
		file.close();
		return true;
	}
	else { return false; }
}

vector<string> FileIO::loadMultiItem(string fileName)
{
	if (!localDirSet) { updateLocalDir(); }
	vector<string> items = vector<string>();
	string fullFilePath = resourcePath + fileName;
	ifstream file(fullFilePath);
	if (file.is_open())
	{
		string item;
		while (getline(file, item))
		{
			items.push_back(item);
		}
	}
	return items;
}

bool FileIO::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}

