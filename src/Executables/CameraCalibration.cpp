﻿//CPP libaries

#include <stdio.h>
#include <mutex>
#include <thread>
#include <regex> 

//Included libraries
#include "BoostSoc.h"
//Note: these headers have to be before any opencv due to a namespace collision (could probably be fixed)

#include <opencv2/opencv.hpp>
//#include <opencv2/imgproc.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

static const int unusedBinocVis = 0;
//Local Libraries
#include "SimpleViewer.h"
#include "StopWatch.h"
#include "StorageObjects.h"
#include "Search_2D.h"
#include "CameraData.h"


//	**************
//	Variables
//	**************
Mat goldClose;
Mat goldFar;

Mat cameraImage;
Mat currentImg;

CameraData* Camera;
const double M_PI = 3.14159265358979323846;

enum StreamImage
{
	siRaw = 1,
	siUndistort = 2,
	siLight = 3,
	siCenter = 4,
	siClose = 5,
	siFar = 6,
	siCanny = 7,
	siHough = 8
};

int imgStream = siRaw;
thread videoThread;

bool programRunning = true;

bool bSave = false;
int saveNumber = 0;

vector<double> lightCorrectMap;

std::chrono::steady_clock::time_point stopWatch;
vector<int> loopTimes;

struct sortClass {
	bool operator() (Vec2f ln1, Vec2f ln2) // r, 0
	{
		float r1 = ln1[0];
		if (ln1[1] > M_PI) { r1 * -1; }
		float r2 = ln2[0];
		if (ln2[1] > M_PI) { r2 * -1; }
		return (r1 < r2);
	}
	bool operator() (cv::Point pt1, cv::Point pt2) { return (pt1.y < pt2.y); }
} sortObject;

// **********************************************************************************************  //
//    Function Declarations                                                                        //
//																										                                             //
// **********************************************************************************************  //
void loadGoldMats();
void loadLight();

void exiting();
void recoverAllAttachedCameras();

void startCamera();
void closeCamera();

void videoStreamThread();

Mat addCenteringLines(Mat displayImg);
Mat compareImages(Mat img1, Mat img2, bool printResults = false);
Mat houghLines(Mat inImage);

void lightCorrect(Mat& image, vector<double>& lightMap);
vector<double> calcLight(Mat image);

Point2i parametricIntersect(float r1, float t1, float r2, float t2);
cv::Mat roiMat(cv::Mat totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax);
void drawHoughLines(Mat Image, vector<Vec2f> lines, Scalar color);

// **********************************************************************************************  //
//    Main					                                                                               //
//																										                                             //
// **********************************************************************************************  //

int loadmain(int argc, char* argv[])
{
	//open the video file for reading
	VideoCapture cap("C:/LocalSoftware/MyVideo.mp4");

	// if not success, exit program
	if (cap.isOpened() == false)
	{
		cout << "Cannot open the video file" << endl;
		cin.get(); //wait for any key press
		return -1;
	}

	//Uncomment the following line if you want to start the video in the middle
	//cap.set(CAP_PROP_POS_MSEC, 300); 

	//get the frames rate of the video
	double fps = cap.get(CAP_PROP_FPS);
	cout << "Frames per seconds : " << fps << endl;

	String window_name = "My First Video";

	namedWindow(window_name, WINDOW_NORMAL); //create a window

	while (true)
	{
		Mat frame;		
		bool bSuccess = cap.read(frame); // read a new frame from video 

		//Breaking the while loop at the end of the video
		if (bSuccess == false)
		{
			cout << "Found the end of the video" << endl;
			break;
		}

		//show the frame in the created window
		imshow(window_name, frame);

		//wait for for 10 ms until any key is pressed.  
		//If the 'Esc' key is pressed, break the while loop.
		//If the any other key is pressed, continue the loop 
		//If any key is not pressed withing 10 ms, continue the loop
		if (waitKey(10) == 27)
		{
			cout << "Esc key is pressed by user. Stoppig the video" << endl;
			break;
		}
	}

	return 0;
}



int savemain(int argc, char* argv[])
{
	////Open the default video camera
	//VideoCapture cap(0);

	//// if not success, exit program
	//if (cap.isOpened() == false)
	//{
	//	cout << "Cannot open the video camera" << endl;
	//	cin.get(); //wait for any key press
	//	return -1;
	//}

	//int frame_width = static_cast<int>(cap.get(CAP_PROP_FRAME_WIDTH)); //get the width of frames of the video
	//int frame_height = static_cast<int>(cap.get(CAP_PROP_FRAME_HEIGHT)); //get the height of frames of the video

	startCamera();
	int frame_width = 800;
	int frame_height = 600;

	Size frame_size(frame_width, frame_height);
	int frames_per_second = 50;

	//Create and initialize the VideoWriter object 
	VideoWriter oVideoWriter("C:/LocalSoftware/MyVideo.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'),
		frames_per_second, frame_size, true);

	//If the VideoWriter object is not initialized successfully, exit the program
	if (oVideoWriter.isOpened() == false)
	{
		cout << "Cannot save the video to a file" << endl;
		cin.get(); //wait for any key press
		return -1;
	}

	string window_name = "My Camera Feed";
	namedWindow(window_name); //create a window called "My Camera Feed"

	while (true)
	{
		if (!Camera) { continue; }
		Mat camFrame = Camera->getLatestImage((imgStream != siRaw));
		Mat frame;

		// Copy edges to the images that will display the results in BGR
		cvtColor(camFrame, frame, COLOR_GRAY2BGR);

		//Mat frame;
		//bool isSuccess = cap.read(frame); // read a new frame from the video camera

		////Breaking the while loop if frames cannot be read from the camera
		//if (isSuccess == false)
		//{
		//	cout << "Video camera is disconnected" << endl;
		//	cin.get(); //Wait for any key press
		//	break;
		//}

		/*
		Make changes to the frame as necessary
		e.g.
		 1. Change brightness/contrast of the image
		 2. Smooth/Blur image
		 3. Crop the image
		 4. Rotate the image
		 5. Draw shapes on the image
		*/

		//write the video frame to the file
		oVideoWriter.write(frame);

		//show the frame in the created window
		imshow(window_name, frame);

		//Wait for for 10 milliseconds until any key is pressed.  
		//If the 'Esc' key is pressed, break the while loop.
		//If any other key is pressed, continue the loop 
		//If any key is not pressed within 10 milliseconds, continue the loop 
		if (waitKey(10) == 27)
		{
			cout << "Esc key is pressed by the user. Stopping the video" << endl;
			break;
		}
	}
	
	//Flush and close the video file
	//oVideoWriter.release();
	return 0;
}


int main(int argc, char** argv)
{
	// On Load
	std::atexit(exiting);	
	startCamera();
	loadGoldMats();	

	stopWatch = steady_clock::now();

	if (!videoThread.joinable()) 
	{ videoThread = std::thread(videoStreamThread); }

	////
	////First load your source image, here load as gray scale
	//cv::Mat srcImage = cv::imread("sourceImage.jpg", CV_LOAD_IMAGE_GRAYSCALE);

	////Then define your mask image
	//cv::Mat mask = cv::Mat::zeros(srcImage.size(), srcImage.type());

	////Define your destination image
	//cv::Mat dstImage = cv::Mat::zeros(srcImage.size(), srcImage.type());

	////I assume you want to draw the circle at the center of your image, with a radius of 50
	//cv::circle(mask, cv::Point(mask.cols / 2, mask.rows / 2), 50, cv::Scalar(255, 0, 0), -1, 8, 0);

	////Now you can copy your source image to destination image with masking
	//srcImage.copyTo(dstImage, mask);
	
	//	Loop
	//
	while (programRunning) 
	{
		int userInput;

		cout << "\n\nSelect calibration: \n"
											<< "0 Save Image\n"
			<< siRaw				<< " Raw\n"
			<< siUndistort	<< " Undistorted\n"
			<< siLight		  << " Light Balanced\n"
			<< siCenter			<< " Centering Lines\n"
			<< siClose			<< " Compare Close\n"
			<< siFar				<< " Compare Far\n"
			<< siCanny			<< " Canny Edge\n"
			<< siHough			<< " Hough Lines\n";
											

		cin >> userInput;

		switch (userInput) 
		{
			case 0:
				bSave = !bSave;
				break;
			//case 9:
			//	calcLight();
			//	break;
			default:
				imgStream = userInput;
		}
	}	
	return 0;
}


// **********************************************************************************************  //
//    				                                                                               //
//																										                                             //
// **********************************************************************************************  //
void loadGoldMats() 
{
	std::string goldFolder = SOLUTION_DIR;
	goldFolder += "../resources/CameraConfigs/";
	goldClose = imread(goldFolder + "goldClose.bmp", IMREAD_GRAYSCALE);
	goldFar = imread(goldFolder + "goldFar.bmp", IMREAD_GRAYSCALE);

	if (!goldClose.empty())
	{
		//imshow("Gold Close", goldClose);
		cout << "Gold Close Loaded\n";
	}
	else { cout << "Gold Close Not Found\n"; }

	if (!goldFar.empty())
	{
		//imshow("Gold Far", goldFar);
		cout << "Gold Far Loaded\n";
	}
	else { cout << "Gold Far Not Found\n"; }
}

void loadLight() 
{

}

void exiting() 
{
	programRunning = false;
	if (videoThread.joinable()) { videoThread.join(); }
	closeCamera();
}

//
//
//
void startCamera() 
{	
	//recoverAllAttachedCameras();
	AVT::VmbAPI::VimbaSystem& sys = AVT::VmbAPI::VimbaSystem::GetInstance();
	//std::cout << "Vimba C++ API Version " << sys << "\n";
	VmbErrorType    err = sys.Startup();
	AVT::VmbAPI::CameraPtrVector cameras;

	std::stringstream strError;

	if (VmbErrorSuccess == err)
	{
		err = sys.GetCameras(cameras); // Fetch all cameras known to Vimba
		if (VmbErrorSuccess == err)
		{
			std::cout << "Cameras found: " << cameras.size() << "\n\n";

			if (cameras.size() == 1) 
			{
				AVT::VmbAPI::CameraPtr pCam = cameras[0];
				err = pCam->Open(VmbAccessModeFull);
				if (VmbErrorSuccess != err) { std::cout << "open camera " << std::to_string(0) << ", error: " << err << " failed\n"; }
				else { std::cout << "camera opened" << std::endl; }

				std::string cameraId;
				err = pCam->GetID(cameraId);
				if (VmbErrorSuccess != err) { std::cout << "get id failed\n"; }

				// pCam->StopContinuousImageAcquisition();
				pCam->Close();

				cameras.clear();
				sys.Shutdown();

				// Creates, loads, and starts asynchronos camera activity
				Camera = new CameraData(cameraId);
			}
			else { 
				cout << "Error! Wrong number of Cameras attached.\nPlease only have one attached.\n"; 
				return;
			}
		}
		else { cout << "Could not list cameras. Error code: " << err << "\n"; }		
	}
	else { std::cout << "Could not start system. Error code: " << err << "\n"; }
}

void closeCamera() 
{		
		Camera->closeCam();
		//delete Camera;	
}

void videoStreamThread() 
{	
	int iCount = 0;
	int lastPictureID = 10000;
	while (programRunning) 
	{
		//Sleep(50); -> WaitKey()v
		if (!Camera) 
		{ 
			Sleep(50);
			continue; 
		}

		stopWatch = steady_clock::now();

		Mat lateImg = Camera->getLatestImage((imgStream != siRaw));		

		if (lateImg.empty()) { continue; }

		int currentPicId = Camera->apiControllerPtr->getImageID();
		if (currentPicId == lastPictureID) 
		{
			Camera->apiControllerPtr->StopContinuousImageAcquisition();
			Camera->apiControllerPtr->ShutDown();
			Camera->apiControllerPtr->~ApiController();
			startCamera();
		}
		lastPictureID = currentPicId;
		
		//ToDo: make camera clipping a variable.
		cameraImage = roiMat(lateImg, Point2i(10, 10), Point2i(780, 580));

		//cameraImage = lateImg;
		Mat displayImg;		
		
		switch (imgStream) 
		{
		case siRaw:
			displayImg = cameraImage;
			break;
		case siUndistort:
			displayImg = cameraImage;
			break;
		case siLight:
			if (lightCorrectMap.empty()) { lightCorrectMap = calcLight(cameraImage); }
			lightCorrect(cameraImage, lightCorrectMap);
			displayImg = houghLines(cameraImage);
			break;
		case siCenter:
			displayImg = addCenteringLines(cameraImage);
			break;
		case siClose:
			displayImg = compareImages(cameraImage, goldClose, false);
			break;
		case siFar:
			displayImg = compareImages(cameraImage, goldFar, false);
			break;
		case siCanny:
			cv::Canny(cameraImage, displayImg, 127, 127, 3, false);
			break;
		case siHough:			
			displayImg = houghLines(cameraImage);
			break;		
		}
		
		if (bSave) 
		{
			std::string saveFile = SOLUTION_DIR;
			saveFile += "../resources/CameraConfigs/";
			saveFile += Camera->camID + "/saveImg" + to_string(saveNumber) + ".bmp";
			imwrite(saveFile, displayImg);
			cout << "Saved as: " << saveFile << "\n";
			saveNumber++;
			bSave = false;
		}
		
		if (displayImg.empty()) { continue; }		

		iCount++;
		loopTimes.push_back((steady_clock::now() - stopWatch).count());
		stopWatch = steady_clock::now();		

		if (iCount > 20) {
			int iSum = 0;
			for (int a = 0; a < loopTimes.size(); a++) 
			{
				iSum += loopTimes[a];
			}
			cout << ((double)iSum / loopTimes.size()) << endl;
			iCount = 0;
			loopTimes.clear();
		}

		imshow("Display ", displayImg);
		if (imgStream != siLight)
		{
			waitKey(50);
		}
		else {
			waitKey(1);
		}

	}
}

Mat addCenteringLines(Mat inImg)
{
	Mat outImg;
	// Copy edges to the images that will display the results in BGR
	cvtColor(inImg, outImg, COLOR_GRAY2BGR);

	int Ys = 0;
	int Xs = 0;
	int Yc = inImg.rows / 2;
	int Xc = inImg.cols / 2;
	int Ye = inImg.rows;
	int Xe = inImg.cols;

	for (int i = 0; i < 5; i++) 
	{
		int a = 75;
		int b = 25;
		line(outImg, Point(Xs, Yc + (i * a)), Point(Xe, Yc + (i * a)), Scalar(0 + (i * b), 50, 255 - (i * b)), 1, LINE_AA);
		line(outImg, Point(Xs, Yc - (i * a)), Point(Xe, Yc - (i * a)), Scalar(0 + (i * b), 50, 255 - (i * b)), 1, LINE_AA);
		line(outImg, Point(Xc + (i * a), Ys), Point(Xc + (i * a), Ye), Scalar(0 + (i * b), 255 - (i * b), 50), 1, LINE_AA);
		line(outImg, Point(Xc - (i * a), Ys), Point(Xc - (i * a), Ye), Scalar(0 + (i * b), 255 - (i * b), 50), 1, LINE_AA);
	}
	return outImg;
}

//
//
//
Mat compareImages(Mat img1, Mat img2, bool printResults)
{
	if(img1.rows != img2.rows || img1.cols != img2.cols){ return Mat(); }

	int countGood = 0;
	int countBad = 0;
	int threshold = 50;

	// 600px * 800px * 255(max) = 122,400,000
	// int max								= 2,147,483,647
	int aveGood = 0;
	int aveBad = 0;
	int aveAll = 0;
	Mat compareMat = cv::Mat(cv::Size(img1.cols, img1.rows), CV_8UC1, Scalar(0));

	//for (int i = 0; i < compareMat.rows; i++)
	//{
	//	for (int j = 0; j < compareMat.cols; j++)
	//	{
	//		int absDiff = abs(img1.at<uchar>(i, j) - img2.at<uchar>(i, j));

	//		aveAll += absDiff;
	//		if (absDiff < threshold) 
	//		{
	//			countGood++;
	//			aveGood += absDiff;
	//		}
	//		else 
	//		{
	//			countBad++;
	//			aveBad += absDiff;
	//		}
	//		compareMat.at<uchar>(i, j) = absDiff;
	//	}
	//}

	uchar* pC = compareMat.data;
	uchar* pI1 = img1.data;
	uchar* pI2 = img2.data;
	for (unsigned int i = 0; i < compareMat.cols * compareMat.rows; i++, pC++, pI1++, pI2++)
	{
		int absDiff = abs(pI1[0] - pI2[0]);

		aveAll += absDiff;
		if (absDiff < threshold)
		{
			countGood++;
			aveGood += absDiff;
		}
		else
		{
			countBad++;
			aveBad += absDiff;
		}		
		pC[0] = absDiff;
	}		

	aveAll /= 480000;
	if(countGood != 0) aveGood /= countGood;
	if(countBad != 0) aveBad /= countBad;

	if (printResults)
	{
		cout << "\nResults: "
			<< "\ncount Good = " << countGood
			<< "\nAve Good = " << aveGood
			<< "\ncount Bad = " << countBad
			<< "\nAve Bad = " << aveBad
			<< "\nAve All= " << aveAll << "\n";
	}
	//Maybe Save to file
	
	return compareMat;
}

//
//
//
Mat houghLines(Mat inImage)
{
	// Variables
	Mat edge, displayImg;

	// Canny Edge
	cv::Canny(inImage, edge, 150, 150, 3, false);

	// Copy edges to the images that will display the results in BGR
	cvtColor(inImage, displayImg, COLOR_GRAY2BGR);

	// Standard Hough Line Transform
	vector<Vec2f> lines; // will hold the results of the detection
	HoughLines(edge, lines, 1, 1.0 * CV_PI / 180, 150, 0, 0); // runs the actual detection
	
	// Sort Horizantal and Vertical
	vector<Vec2f> vertLines; // r, 0
	vector<Vec2f> horzLines;	
	
	double horzLow1 = 45 * M_PI / 180;
	double horzHigh1 = 135 * M_PI / 180;
	double horzLow2 = 225 * M_PI / 180;
	double horzHigh2 = 315 * M_PI / 180;

	for (int i = 0; i < lines.size(); i++) 
	{
		if ((lines[i][1] < horzHigh1 && lines[i][1] > horzLow1) 
			|| (lines[i][1] < horzHigh2 && lines[i][1] > horzLow2))
		{ horzLines.push_back(lines[i]); }
		else 
		{ vertLines.push_back(lines[i]); }		
	}
	
	if (vertLines.size() < 2 || horzLines.size() < 2) { return displayImg; }

	// Sort Lines by Size
	std::sort(horzLines.begin(), horzLines.end(), sortObject);
	std::sort(vertLines.begin(), vertLines.end(), sortObject);

	vector<Vec2f>::iterator it1 = horzLines.begin();	
	for (vector<Vec2f>::iterator it2 = it1 + 1; it2 != horzLines.end(); /*it1++ it2++*/)
	{
		if ((abs(it2->val[0]) - abs(it1->val[0])) < 35)
		{
			double d01 = it1->val[1];

			if (d01 > M_PI) { d01 -= M_PI; }
			double d02 = it2->val[1];
			if (d02 > M_PI) { d02 -= M_PI; }

			if (abs(d01 - (M_PI/2)) > abs(d02 - (M_PI / 2)) )
			{
				it1 = horzLines.erase(it1);
				it2++; //it2 = it1 + 1; // NOTe
			}
			else // abs(d01 - (M_PI/2)) <= abs(d02 - (M_PI/2))
			{
				it2 = horzLines.erase(it2);
				//it2 = it1 + 1;
			}
		}
		else
		{
			it1++;
			it2++;
		}
	}

	// Erase duplicate lines too close
	it1 = vertLines.begin();
	for (vector<Vec2f>::iterator it2 = it1 + 1; it2 != vertLines.end(); /*it1++ it2++*/)
	{
		if ((abs(it2->val[0]) - abs(it1->val[0])) < 35)
		{
			double d01 = it1->val[1];

			if (d01 > M_PI) { d01 -= M_PI * 2; }
			double d02 = it2->val[1];
			if (d02 > M_PI) { d02 -= M_PI * 2; }

			if (abs(d01) > abs(d02))
			{
				it1 = vertLines.erase(it1);
				it2 = it1 + 1;
			}
			else // abs(d01) <= abs(d02)
			{
				it2 = vertLines.erase(it2);
				//it2 = it1 + 1;
			}
		}
		else
		{
			it1++;
			it2++;
		}
	}

	drawHoughLines(displayImg, horzLines, Scalar(0, 0, 255));
	drawHoughLines(displayImg, vertLines, Scalar(255, 0, 0));

	// Line Stats	
	Scalar colorR(0, 180, 180);
	Scalar colorO(205, 205, 0);

	Point2i horzR(5, -15);
	Point2i horzO(displayImg.cols - 50, -15);
	putText(displayImg, "r", Point(5, 15), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, colorR);
	for (size_t i = 0; i < horzLines.size(); i++)
	{
		Point2i pR = horzR;
		Point2i pO = horzO;
		pR.y += abs(horzLines[i][0]);
		pO.y += abs(horzLines[i][0]);

		std::stringstream ssR;
		ssR << std::fixed << std::setprecision(0) << horzLines[i][0];
		std::string sR = ssR.str();

		std::stringstream ssO;
		ssO << std::fixed << std::setprecision(1) << (horzLines[i][1] * 180 / M_PI);
		std::string sO = ssO.str();

		putText(displayImg, sR, pR, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, colorR);
		putText(displayImg, sO, pO, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, colorO);		
	}
	
	Point2i vertR(5, 15);
	Point2i vertO(-30, displayImg.rows - 10);
	putText(displayImg, "O", Point(displayImg.cols - 25, displayImg.rows - 10), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, colorO);
	for (size_t i = 0; i < vertLines.size(); i++)
	{
		Point2i pR = vertR;
		Point2i pO = vertO;
		pR.x += abs(vertLines[i][0]);
		pO.x += abs(vertLines[i][0]);

		std::stringstream ssR;
		ssR << std::fixed << std::setprecision(0) << vertLines[i][0];
		std::string sR = ssR.str();

		std::stringstream ssO;
		ssO << std::fixed << std::setprecision(1) << (vertLines[i][1] * 180 / M_PI);
		std::string sO = ssO.str();

		putText(displayImg, sR, pR, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, colorR);
		putText(displayImg, sO, pO, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, colorO);
	}

	// Checker Intensities
	for (int i = 0; i < vertLines.size() - 1; i++) 
	{
		for (int j = 0; j < horzLines.size() - 1; j++) 
		{
			Point2i Pi = parametricIntersect(vertLines[i][0], vertLines[i][1], horzLines[j][0], horzLines[j][1]);
			Point2i Pf = parametricIntersect(vertLines[i + 1][0], vertLines[i + 1][1],
				horzLines[j + 1][0], horzLines[j + 1][1]);

			if (Pi == Point2i() || Pf == Point2i()) { continue; } // No intersect

			Pi.x += 10;
			Pi.y += 10;
			Pf.x -= 10;
			Pf.y -= 10;

			int xDis = Pf.x - Pi.x;
			int yDis = Pf.y - Pi.y;
			
			if( xDis < 0 || yDis < 0 || ( yDis < 1 *(xDis - yDis)) ||	(xDis < 1 * (yDis - xDis)) ) // Not squarish 2X4 off
			{ continue; }
			
			int iIntensity = 0;
			for (int x = Pi.x; x <= Pf.x; x++)
			{
				for (int y = Pi.y; y <= Pf.y; y++)
				{
					if (x < 0 || x > inImage.cols || y < 0 || y > inImage.rows) { continue; }
					iIntensity += inImage.at<uchar>(y, x);
				}
			}
			iIntensity /= ((xDis + 1) * (yDis + 1));

			rectangle(displayImg, Pi, Pf, Scalar(0, 255, 0), 1, LINE_AA);
			putText(displayImg, to_string(iIntensity) , cv::Point(Pi.x + 10, Pi.y + yDis / 2 - 10), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, Scalar(205,0,205));			
		}
	}

	// return results
	return displayImg;
}

void drawHoughLines(Mat Image, vector<Vec2f> lines, Scalar color)
{
	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a * rho, y0 = b * rho;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(Image, pt1, pt2, color, 1, LINE_AA);
	}
}

//Find point (x,y) where two parameterized lines intersect :p Returns 0 if lines are parallel 
Point2i parametricIntersect(float r1, float t1, float r2, float t2) 
{
	int x, y;
	float ct1 = cosf(t1);     //matrix element a
	float st1 = sinf(t1);     //b
	float ct2 = cosf(t2);     //c
	float st2 = sinf(t2);     //d
	float d = ct1 * st2 - st1 * ct2;        //determinative (rearranged matrix for inverse)
	if (d != 0.0f) {
		x = (int)((st2 * r1 - st1 * r2) / d);
		y = (int)((-ct2 * r1 + ct1 * r2) / d);
		return(Point2i(x,y));
	}
	else { //lines are parallel and will NEVER intersect!
		return(Point2i());
	}
}

//
//
//
void lightCorrect(Mat& image, vector<double>& lightMap)
{
	uchar* pI = image.data;
	vector<double>::iterator pV = lightMap.begin();

	for (int i = 0; i < image.cols * image.rows; i++, pI++, pV++)
	{		
		//pI[0] = min((int)(pV[0] * pI[0]), 255);
		//ToDo: verify
		*pI = min((int)(*pV * *pI), 255);
	}
}

//
//
//
vector<double> calcLight(Mat image)
{
	double gain = 0.0002;
	double start = 0.90;
	int iC = (image.rows / 2);
	int jC = (image.cols / 2);

	vector<double> lightMap(image.rows * image.cols);
	int k = 0;

	for (int i = 0; i < image.rows; ++i)
	{
		uchar* pixel = image.ptr<uchar>(i);  // point to first color in row
		for (int j = 0; j < image.cols; j++, pixel++)
		{

	//for (int i = 0; i < image.rows; i++)
	//{
	//	for (int j = 0; j < image.cols; j++)
	//	{
			double distance = sqrt(pow((iC - i) * 6, 2) * 1 + pow((jC - j) * 8, 2)); // 800X600 so * opposite
			double val = ((gain * distance) + start);
			lightMap[k] = val;
			k++;			
		}
	}
	return lightMap;
}


//Mat calcLight(Mat inImage)
//{
//
//	Mat outImg = cv::Mat(cv::Size(inImage.cols, inImage.rows), CV_8UC1, Scalar(0));
//
//	double gain = 0.0002;
//	double start = 0.90;
//	int iC = (inImage.rows / 2);
//	int jC = (inImage.cols / 2);
//
//	vector<double> dCount(inImage.rows * inImage.cols);		
//	int k = 0;
//	for (int i = 0; i < inImage.rows; i++)
//	{
//		for (int j = 0; j < inImage.cols; j++)
//		{
//			double distance = sqrt(pow((iC - i) * 6, 2) * 1 + pow((jC - j) * 8, 2)); // 800X600 so * opposite
//			double val = ((gain * distance) + start);
//			dCount[k] = val;
//			k++;
//			outImg.at<uchar>(i, j) = min((int)(val * inImage.at<uchar>(i, j)), 255);
//		}
//	}
//	return outImg;
//}

//creates a mini roi mat (inclusive) of boundaries
/// <summary> roi (Region of Interest) returns a mat clipped to input points </summary>
/// <param name="ptMin"> inclusive of boundary </param>
/// <param name="ptMax"> inclusive of boundary </param>
Mat roiMat(cv::Mat totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax)
{
	//bound to protect from OB of totalMatrix
	ptMin.x = std::max(0, ptMin.x);
	ptMin.y = std::max(0, ptMin.y);
	ptMax.x = std::min(totalMat.cols - 1, ptMax.x);
	ptMax.y = std::min(totalMat.rows - 1, ptMax.y);

	/*if (ptMax.x < 0 || ptMax.y < 0) { ptMax.x = 0; ptMax.y = 0; std::cout << "Error\n"; }*/

	Mat miniMat = Mat(cv::Size(ptMax.x - ptMin.x + 1, ptMax.y - ptMin.y + 1), CV_8UC1);
	for (int i = ptMin.x; i <= ptMax.x; i++)
	{
		for (int ii = ptMin.y; ii <= ptMax.y; ii++)
		{
			miniMat.at<cv::uint8_t>(ii - ptMin.y, i - ptMin.x) = totalMat.at<cv::uint8_t>(ii, i);
		}
	}
	return miniMat;
}
