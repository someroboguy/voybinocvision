//General libraries
#include <cstdlib>
#include <iostream>
#include <boost/smart_ptr.hpp>
#include <boost/thread/thread.hpp>

//Project libraries
#include <BoostSoc.h>
//std::vector<std::string> splitStr(std::string msg, char delimiter);
void readIncomingMessages(BoostSoc* connectedSoc);
std::string socType = "";
int port = 2001;
std::string serverIP = "";
std::string portStr = "";
std::string incomingPrints = "";
int main()
{
	std::cout << "C: for client, S: for server" << std::endl;
	std::getline(std::cin, socType);

	std::cout << "print incoming: Y/N" << std::endl;
	std::getline(std::cin, incomingPrints);


	//BoostSoc chatSocket(false);
	BoostSoc chatSocket(false);
	if (socType == "S")
	{
		std::cout << "Enter port:" << std::endl;
		std::getline(std::cin, portStr);
		port = std::stoi(portStr);

		std::cout << "listening for client" << std::endl;
		chatSocket.serverConnect(port, true, true);
		std::cout << "client connected, chat active" << std::endl;
	}
	else if(socType == "C")
	{
		std::cout << "Enter ip of server:" << std::endl;
		std::getline(std::cin, serverIP);

		std::cout << "Enter port:" << std::endl;
		std::getline(std::cin, portStr);
		port = std::stoi(portStr);

		std::cout << "attempting to connect to server" << std::endl;
		chatSocket.clientConnect(port, serverIP, true, true);
		std::cout << "server connected, chat active" << std::endl;
	}
	else
	{
		std::cout << "incorrect selection, program terminating" << std::endl;
		system("pause");
		return 1;
	}

	//chat manager top
	if (incomingPrints == "Y")
	{
		boost::thread readThread(boost::bind(readIncomingMessages, &chatSocket));
	}
	
	while (true)
	{
		std::string outMessage;
		std::cout << "Me: ";
		std::getline(std::cin, outMessage);
		chatSocket.sendMessage(outMessage);
	}

	system("pause");
}

void readIncomingMessages(BoostSoc* connectedSoc)
{
	bool conDroppedFlag = false;
	while (true)
	{
		std::string inMessage = connectedSoc->getNextMessage();
		if (inMessage != "")
		{
			std::cin.clear();
			std::cout << "\r"; //clears current line on cout
			std::cout << "Friend: ";
			std::cout << inMessage << std::endl;
			std::cout << "Me: ";// +currentInput;
		}
		if (!connectedSoc->getConnected() && !conDroppedFlag)
		{
			conDroppedFlag = true;
			std::cin.clear();
			std::cout << "\r"; //clears current line on cout
			std::cout << "Connection Dropped..." << std::endl;
		}
		else if (connectedSoc->getConnected() && conDroppedFlag)
		{
			conDroppedFlag = false;
			std::cin.clear();
			std::cout << "\r"; //clears current line on cout
			std::cout << "Connection Recovered!" << std::endl;
			std::cout << "Me: ";// +currentInput;
		}
	}
}