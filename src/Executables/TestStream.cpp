#include <string>

//CPP libaries
#include <stdio.h>
#include <mutex>
#include <thread>

//Included libraries
#include "BoostSoc.h"
//Note: these headers have to be before any opencv due to a namespace collision (could probably be fixed)
#include "ApiController.h"

#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

static const int unusedBinocVis = 0;
//Local Libraries
#include "SimpleViewer.h"
#include "StopWatch.h"
#include "StorageObjects.h"
#include "Search_2D.h"
BoostSoc receiveSoc(false, 1);
bool continueStream = true;

void stringToImage(std::string inString, cv::Mat& imageMat, int imageWidth = 800, int imageHeight =  600);
void stringToImageByRow(cv::Mat& imageMat, int imageWidth = 800, int imageHeight = 600);
int main(int argc, char** argv)
{
	receiveSoc.clientConnect(10500, "127.0.0.1", true);
	cv::Mat displayImage = Mat(cv::Size(800, 600), CV_8UC1);
	while (continueStream) {
		stringToImageByRow(displayImage);
		imshow("Cam1" , displayImage);
		waitKey(20);
	}
	destroyAllWindows();
	
}

void stringToImage(std::string inString, cv::Mat& imageMat, int imageWidth, int imageHeight) {
	std::vector<std::string> splitStrings = BoostSoc::splitStr(inString, ',');
	
	for (int i = 1; i < splitStrings.size() - 1; i++) {
		int row = i / imageHeight;
		int column = i % imageHeight;
		imageMat.at<cv::uint8_t>(row, column) = std::stoi(splitStrings[i]);
	}
}

void stringToImageByRow(cv::Mat& imageMat, int imageWidth, int imageHeight) {
	std::vector<std::string> allMessages = receiveSoc.getMessages(800,true);

	for (int i = 0; i < allMessages.size(); i++){
		std::string currentRow = allMessages.at(i);

		if (currentRow.size() != (imageWidth+3)) { continue; }
				
		int row = std::stoi(currentRow.substr(0, 3));
		
		for (int j = 3; j < currentRow.size() - 3; j++) {
			int pixelValue = (int)currentRow[j];
			imageMat.at<cv::uint8_t>(row, j) = pixelValue;
		}
	}
}