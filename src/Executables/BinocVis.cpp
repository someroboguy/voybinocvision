/////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////						Notes			              /////////////////////////////////////////
//		OpenCV is Mat image.at<uchar>(row,col) / (y,x) / (height, width)
//			
//		All other classes are (x,y) -> as they have no coorelation to row/col but are the standard of
//		(Point, Circle, Rect)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////


//CPP libaries

#include <stdio.h>
#include <mutex>
#include <thread>
#include <regex> 

//Included libraries
#include "BoostSoc.h"
//Note: these headers have to be before any opencv due to a namespace collision (could probably be fixed)

#include <opencv2/opencv.hpp>
//#include <opencv2/imgproc.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

static const int unusedBinocVis = 0;
//Local Libraries
#include "SimpleViewer.h"
#include "StopWatch.h"
#include "StorageObjects.h"
#include "Search_2D.h"
#include "CameraData.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////    Hardware Settings               /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool singleCameraMode = false;
bool distortionCorrection = true;

bool calibrationMode = false;
//bool playbackStore = true;

// Debug Stuff
bool fakeSerial = false;
int loopCount = 100;
const bool debugMode = false;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	**************
//	Debug
//	**************
std::vector<std::vector<double>> allTimes;
std::vector<double>loopTimes(3);
std::mutex mAllTime;

//	**************
//	Constants
//	**************
const int MIN_TARGET_RADIUS = 20;//px
const int MAX_TARGET_RADIUS = 55;//px

const int SERIAL_MS = 15;
const int CALMODE_DELAY = 15;// 000;//

int TARGET_COLOR_THRESHOLD = 60;
int COLOR_CHECK_PADDING = 3; //NOTE: this padding must be less than the box padding

//	**************
//	Variables
//	**************
int boxPadding = 5; //this relates to max speed an object can be tracked at (max motion of object between 2 tracked frames)
int radPadding = 2;	//this should be linearly correlated with above...

bool terminateProgram = false;
bool continueThreadedTracking = true;
enum ProgramStates { psInitialize = 0, psSerialSetup = 1, psStartThreads = 2, psConfirmSerial = 11, psRunning = 3, psReset = 4 };
int programState = psInitialize;

std::vector<CameraData*> Cameras;
std::chrono::steady_clock::time_point stopWatch;
std::mutex cameraUpdateMutex;

std::vector<cv::Point2i> clickedPoints{ cv::Point2i(), cv::Point2i() };

int maxTrackedTargets = 9;// 9;
long targetTimeout = 1000000; //uS

std::vector<std::thread> trackingThreadPool;
std::thread hunterThread;
std::thread streamThread;
std::thread videoThread;

BoostSoc serialOut(false, SERIAL_MS);
// because no copy constructor
BoostSoc vidStream1(false, 50);
BoostSoc vidStream2(false, 50);
//std::vector<BoostSoc*> videoStream{ &vidStream1, &vidStream2 };

enum VideoPlayer { vpNone = 0, vpLocal = 1, vpRemote = 2 };
int videoPlayer = vpLocal;
int lastVideoPlayer = videoPlayer;

// **********************************************************************************************  //
//    Function Declarations                                                                        //
//																										                                             //
// **********************************************************************************************  //
void serialComms();
void logData();

void startCameras();
void closeCameras();

void videoStreamThread();
void serializeCameraFrameByRow(int cameraID, cv::Mat& cameraImage, BoostSoc* sendSoc);

void threadedTargetHunter();
void threadedTrackTarget(int cameraIndex);

cv::Mat roiMat(cv::Mat totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax);

// **********************************************************************************************  //
//    Main					                                                                               //
//																										                                             //
// **********************************************************************************************  //
int main(int argc, char** argv)
{
	std::cout << "Opening Connections" << endl;
	vidStream1.serverConnect(10500, false);
	vidStream2.serverConnect(10501, false);
	std::cout << "Starting Loop" << endl;

	remove("logging.csv");

	while (!terminateProgram)
	{
		Sleep(SERIAL_MS);
		std::string msg;
		//std::cout << programState << std::endl;//Debug
		switch (programState)
		{
		case(psInitialize)://initialize cameras
			std::cout << "Starting Cams" << std::endl;
			startCameras();
			//reset runtime variables
			if (videoPlayer == vpNone) { videoPlayer = vpLocal; }
			if (fakeSerial) { programState = psStartThreads; }
			else { programState = psSerialSetup; }
			break;

		case(psSerialSetup)://Serial setup
			std::cout << "listening for serial connection: " << "port: 2003" << std::endl;
			if (!serialOut.getConnected() && !serialOut.getListening())
			{
				serialOut.serverConnect(2003, true, true); //connection thread needs to be initialized
			}
			else //connection thread is working, just need to wait for it to succeed
			{
				while (!serialOut.getConnected()) { Sleep(500); }
			}

			std::cout << "serial connected" << std::endl;
			programState = psStartThreads;
			break;

		case(psStartThreads)://Thread Spinoff			
			if (videoPlayer != vpNone ) { videoThread = std::thread(videoStreamThread); } // && !videoThread.joinable()
			programState = psRunning;
			if (debugMode) { threadedTargetHunter(); }
			else 
			{ 
				continueThreadedTracking = true;
				hunterThread = std::thread(threadedTargetHunter); 
				for (int i = 0; i < Cameras.size(); i++)
				{
					trackingThreadPool.push_back(std::thread(threadedTrackTarget, i));
				}
			}
			break;

		case(psRunning)://Continue until serial kill commmand or user input x
		{
			serialComms();
			break;
		}
		case(psReset)://Cleanup, program exit
			int tVidPlay = videoPlayer;
			videoPlayer = vpNone;
			if (videoThread.joinable()) { std::cout << "Joining UI" << std::endl; videoThread.join(); }
			videoPlayer = tVidPlay;
			if (hunterThread.joinable()) { std::cout << "Hunter Join" << std::endl; hunterThread.join(); }
			continueThreadedTracking = false;
			for (int i = 0; i < trackingThreadPool.size(); i++)
			{
				if (trackingThreadPool.at(i).joinable()) { trackingThreadPool.at(i).join(); }
			}			
			closeCameras();
			//serialOut.disconnectSocket();
			programState = psInitialize;
			Sleep(1000);
			std::cout << "Reset Complete" << std::endl;
			break;
		}
	}
}

// **********************************************************************************************  //
//    Serial				                                                                               //
//																										                                             //
// **********************************************************************************************  //
void serialComms()
{
	std::vector<std::string> msg = serialOut.getMessages(-1, true);
	if (msg.size() != 0)
	{
		for (int k = 0; k < msg.size(); k++) {
			std::vector<std::string> msgs = BoostSoc::splitStr(msg[k], ',');
			if (msgs[0] == "stopStream")
			{
				programState = psReset;
			}
			else if (msgs[0] == "TC" && msgs.size() == 2)
			{
				maxTrackedTargets = std::stoi(msgs[1]);
				std::cout << "Tracked target count set to: " << maxTrackedTargets << std::endl;
			}
			else if (msgs[0] == "VIS" && msgs.size() == 2)
			{
				if (msgs[1] == "0")
				{
					videoPlayer = vpNone;
					std::cout << "Visualizer stopped" << std::endl;
				}
				else if (videoPlayer != vpLocal)
				{
					videoPlayer == vpLocal;
					if (!videoThread.joinable()) { videoThread = std::thread(videoStreamThread); }
				}
			}
			else if (msgs[0] == "startCalibrateMode") {
				videoPlayer = vpRemote;
			}
			else if (msgs[0] == "stopCalibrateMode") {
				videoPlayer = vpLocal;
			}

			else if (msgs[0] == "multiMask") {
				for (int i = 1; i < msgs.size(); i += 7) {
					vector<Point2i> points;								
					int camNum = std::stoi(msgs[i]);
					for (int j = 0; j < 6; j += 2)
					{						
						points.push_back(Point2i(std::stoi(msgs.at(i + j + 0)), std::stoi(msgs.at(i + j + 1))));
					}					
					Cameras[camNum]->setPolyMask(points, true, 255);					
				}
			}
			else if (msgs[0] == "multiMaskTCP") {
				for (int i = 1; i < msgs.size(); i += 9) {
					vector<Point2i> points;
					int camNum = std::stoi(msgs[i]);
					for (int j = 0; j < 8; j += 2)
					{
						points.push_back(Point2i(std::stoi(msgs.at(i + j + 0)), std::stoi(msgs.at(i + j + 1))));
					}
					Cameras[camNum]->setPolyMask(points, true, 255);
				}
			}
			else if (msgs[0] == "maskAll") {
				std::cout << "Masking all\n" << std::endl;
				for (int i = 1; i < msgs.size(); i++) {
					try {
						Cameras[std::stoi(msgs[i])]->resetMask(255);
					}
					catch (...) {
						std::cout << "Invalid camera index" << std::endl;
					}
				}
			}
			else if (msgs[0] == "unmaskAll") {
				std::cout << "Unmasking all\n" << std::endl;
				for (int i = 0; i < Cameras.size(); i++) {
					try {
						Cameras[i]->resetMask(-1);
					}
					catch (...) {
						std::cout << "Invalid camera " << std::endl;
					}
				}
			}
			else if (msgs[0] == "changeNumTargets") {
				std::cout << "Changing number of targets.\n";
				maxTrackedTargets = std::stoi(msgs[1]);
			}
			else if (msgs[0] == "savePlaybackFrames")
			{
				std::cout << "Saving playback frames";
				std::string nameString = msgs[1];
				for (int i = 0; i < 2; i++) {
					Cameras[i]->playbackStore = false;
					Cameras[i]->savePlayback("cam" + to_string(i) + "-" + nameString, 0.03);
					Cameras[i]->playbackStore = true;
				}				
			}
			else if (msgs[0] == "pausePlaybackStore")
			{
				Cameras[0]->playbackStore = false;
				Cameras[1]->playbackStore = false;
			}
			else if (msgs[0] == "resumePlaybackStore")
			{
				Cameras[0]->playbackStore = true;
				Cameras[1]->playbackStore = true;
			}
			else if (msgs[0] == "calibrationModeOn")
			{
				//boxPadding = 15; 
				//radPadding = 6;
				calibrationMode = true;
			}
			else if (msgs[0] == "calibrationModeOff")
			{
				calibrationMode = false;
				//boxPadding = 5;
				//radPadding = 2;
			}
		}
	}

	if (!serialOut.getConnected() && !fakeSerial)
	{
		std::cout << "serial dropped..." << std::endl;
		//programState = psReset;
	}
}
// **********************************************************************************************  //
//		Logging				                                                                               //
//																										                                             //
// **********************************************************************************************  //
void logData()
{
	mAllTime.lock();
	std::vector<std::vector<double>> someTimes(allTimes.size());
	for (int i = 0; i < allTimes.size(); i++) {
		someTimes[i] = allTimes[i];
	}
	allTimes.clear();
	mAllTime.unlock();

	std::ofstream myfile;
	myfile.open("logging.csv", std::ios::app);

	for (int i = 0; i < someTimes.size(); i++) {
		for (int j = 0; j < someTimes[i].size(); j++) {
			myfile << someTimes[i][j] << ",";
		}
		myfile << endl;
	}
	myfile.close();
}

// **********************************************************************************************  //
//    Cameras				                                                                               //
//																										                                             //
// **********************************************************************************************  //
void startCameras()
{
	AVT::VmbAPI::VimbaSystem& sys = AVT::VmbAPI::VimbaSystem::GetInstance();
	//std::cout << "Vimba C++ API Version " << sys << "\n";
	VmbErrorType    err = sys.Startup();
	AVT::VmbAPI::CameraPtrVector cameras;

	std::stringstream strError;

	if (VmbErrorSuccess == err)
	{
		err = sys.GetCameras(cameras); // Fetch all cameras known to Vimba
		if (VmbErrorSuccess == err)
		{
			std::cout << "Cameras found: " << cameras.size() << "\n\n";

			for (int i = 0; i < cameras.size(); i++)
			{
				std::string cameraId;
				err = cameras[i]->GetID(cameraId);
				if (VmbErrorSuccess != err) { std::cout << "get id failed\n"; }

				// Creates, loads, and starts asynchronos camera activity
				CameraData* fC = new CameraData(cameraId);
				Cameras.push_back(fC);
				fC->startCam();
			}
		}
		else { std::cout << "Could not list cameras. Error code: " << err << "\n"; }
	}
	else { std::cout << "Could not start system. Error code: " << err << "\n"; }
}

void closeCameras()
{
	for (int i = 0; i < Cameras.size(); i++)
	{
		Cameras[i]->closeCam();
		delete Cameras[i];
	}
}


std::vector<std::vector<std::string>> targetFreqs(2);
void videoStreamThread()
{
	int ind0 = 0;
	int ind1 = 1;

	for (int i = 0; i < Cameras.size(); i++)
	{
		std::string windowName = "Cam " + std::to_string(i);
		namedWindow(windowName);
	}

	while (videoPlayer != vpNone)
	{
		if (videoPlayer == vpLocal)
		{
			//std::vector<double> loopTimes(25);

			for (int i = 0; i < Cameras.size(); i++)
			{
				if (!Cameras[i]) { continue; }
				Mat displayImage = Cameras[i]->getLatestImage();
				//Mat displayImage;
				//cv:threshold(tImage, displayImage, 195, 255, 0);
				if (displayImage.empty()) { continue; }
				int frequency = Cameras[i]->apiControllerPtr->getCaptureFrequency();
				int textPos = 1;

				cv::putText(displayImage, "Capture Frequency: " + std::to_string(frequency) + " Hz", cv::Point(0, 15 * textPos++), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, 255);

				std::vector<trackedTarget> camTargets = Cameras[i]->getCamTargets();
				//loopCount++;
				//if (loopCount > 12)
				{
					//logData();
					targetFreqs[i].clear();
					for (int ii = 0; ii < camTargets.size(); ii++)
					{						
						std::stringstream stream;
						stream << std::fixed << std::setprecision(2) << "Target #: " << std::to_string(ii)
							<< ", Freq: " << camTargets.at(ii).getTrackedFrequency() << " Hz";
						targetFreqs[i].push_back(stream.str());
					}
					// loopCount = 0;
				}
				for (int ii = 0; ii < camTargets.size(); ii++)
				{
					//loopTimes[ii] = camTargets.at(ii).getTrackedFrequency();
					circle2D temp = camTargets.at(ii).getLastLocation();
					Search_2D::drawCircle(displayImage, cv::Point2f(temp.x, temp.y), temp.rad, 255); // 255
					if (targetFreqs[i].size() > ii)
					{
						cv::putText(displayImage, targetFreqs[i][ii], cv::Point(0, 15 * textPos++), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.0, 255);
					}
				}

				//// Speed Tests
				//mAllTime.lock();
				//allTimes.push_back(loopTimes);
				//mAllTime.unlock();

				imshow("Cam " + std::to_string(i), displayImage);
			}
			waitKey(SERIAL_MS);
		}
		else // vpRemote
		{
			for (int i = 0; i < Cameras.size(); i++)
			{
				Mat displayImage = Cameras[i]->getLatestImage();
				if (i == 0) { serializeCameraFrameByRow(i, displayImage, &vidStream1); }
				else if (i == 1) { serializeCameraFrameByRow(i, displayImage, &vidStream2); }
			}
			Sleep(50);
		}
	}

	destroyAllWindows();
	std::cout << "debug exit" << std::endl;
}


void serializeCameraFrameByRow(int cameraID, cv::Mat& cameraImage, BoostSoc* sendSoc) {
	int rows = cameraImage.rows;
	int cols = cameraImage.cols;

	for (int i = 0; i < rows; i++) {
		std::string serializedFrame = "";
		if (i < 10) { serializedFrame = "00" + std::to_string(i); }
		else if (i < 100) { serializedFrame = "0" + std::to_string(i); }
		else { serializedFrame = std::to_string(i); }

		for (int j = 0; j < cols; j++) {  //cols
			char encodedPixel = cameraImage.at<cv::uint8_t>(i, j);
			if (encodedPixel == ';') { encodedPixel = '9'; };
			serializedFrame += encodedPixel;
		}
		sendSoc->sendMessage(serializedFrame, false);
	}
}

// **********************************************************************************************  //
//    Threads				                                                                               //
//																										                                             //
// **********************************************************************************************  //
void threadedTargetHunter()
{
	while (programState == psRunning)
	{
		Sleep(250);
		for (int i = 0; i < Cameras.size(); i++)
		{
			//trim dead targets			
			std::vector<trackedTarget> camTargets = Cameras[i]->getCamTargets();
			for (int ii = 0; ii < camTargets.size(); ii++)
			{
				if (!camTargets[ii].getTrackingActive() && camTargets[ii].microsSinceLastFound() > targetTimeout)
				{
					std::cout << "Target Dropped: " << std::to_string(ii) << std::endl;
					Cameras[i]->removeCamTarget(camTargets[ii].getUID());
				}				
			}

			//removes overlapping tracked targets			
			camTargets = Cameras[i]->getCamTargets();
			for (int ii = 0; ii < camTargets.size(); ii++) {
				for (int j = ii + 1; j < camTargets.size(); j++) {
					circle2D curTarget = camTargets[ii].getLastLocation();
					circle2D compTarget = camTargets[j].getLastLocation();
					if (circle2D::distance(curTarget, compTarget) < (curTarget.rad / 4)) {
						Cameras[i]->removeCamTarget(camTargets[j].getUID());
						std::cout << "Target 2 Dropped: " << std::to_string(j) << std::endl;
						break;
					}
				}				
			}		

			//if open target slots, search for new ones
			camTargets = Cameras[i]->getCamTargets();
			if (camTargets.size() < maxTrackedTargets)
			{				
				Mat image = Cameras[i]->getLatestImage();
				if (image.empty()) { continue; }

				Search_2D fastSearch = Search_2D(image.cols, image.rows);

				Mat currentCamMask = Cameras[i]->getMaskMat();
				std::vector<long> loopTimes(3);
				std::vector<circle2i> circles = fastSearch.circlePipelineFullImage( image, currentCamMask, camTargets);

				//new target found, spin off thread to track it.
				if (!debugMode)
				{
					for (int ii = 0; ii < circles.size() && camTargets.size() + ii < maxTrackedTargets; ii++)
					{
						trackedTarget newTarg(circles.at(ii));
						Cameras[i]->addCamTarget(newTarg);
						//trackingThreadPool.push_back(std::thread(threadedTrackTarget, i, newTarg.getUID()));
					}					
				}
			}
			/*else
			{
				Sleep(250);
			}*/
		}
	}
}

//
//
//
void threadedTrackTarget(int cameraIndex)
{
	int pictureID = 0;

	// Loop
	while (continueThreadedTracking)
	{
		// Calibration Mode
		//if (calibrationMode) { Sleep(CALMODE_DELAY); }
		if (debugMode) { Sleep(2000); }

		// Gets new image, if no new image reloop
		int currentPicId = Cameras[cameraIndex]->apiControllerPtr->getImageID();
		if (currentPicId == pictureID) { continue; }
		else { pictureID = currentPicId; }
		Mat image = Cameras[cameraIndex]->getLatestImage();

		// Get target if still valid
		//trackedTarget threadTarget = Cameras[cameraIndex]->getCamTarget(targetUID);
		//if (threadTarget.stopTracking) { return; }

		std::vector<trackedTarget> camTargets = Cameras[cameraIndex]->getCamTargets();
		if (camTargets.empty()) { continue; }
		for (int i = 0; i < camTargets.size(); i++)
		{
			//std::chrono::steady_clock::time_point lastUpdate = std::chrono::steady_clock::now();
			
			// Makes a small Image centerered around where target was last.
			circle2D targLast = camTargets[i].getLastLocation();
			Point2i minPt = Point2i(targLast.x - (targLast.rad + boxPadding), targLast.y - (targLast.rad + boxPadding));
			Point2i maxPt = Point2i(targLast.x + (targLast.rad + boxPadding), targLast.y + (targLast.rad + boxPadding));

			//Rect roiRect(minPt, maxPt);
			//Mat miniMat = image(roiRect);		
			targLast.x -= minPt.x;
			targLast.y -= minPt.y;


			Mat miniMat = roiMat(image, minPt, maxPt);
			//Mat currentCamMask = roiMat(Cameras[cameraIndex]->getMaskMat(), minPt, maxPt);		*/

			// Create Search Obj
			Search_2D fastSearch = Search_2D(miniMat.cols, miniMat.rows);
			int minRad = max(MIN_TARGET_RADIUS, (int)targLast.rad - radPadding);
			int maxRad = min(MAX_TARGET_RADIUS, (int)targLast.rad + radPadding);

			// Do Search	
			circle2D targNew = fastSearch.circlePipelineROI(miniMat, targLast, minRad, maxRad);

			if (targNew.rad < MIN_TARGET_RADIUS || targNew.rad > MAX_TARGET_RADIUS)
			{
				camTargets[i].updateTrackedTarget();
			}
			else
			{
				//cout << shiftedCircle.print() << endl;
				targNew.x += minPt.x;
				targNew.y += minPt.y;
				camTargets[i].updateTrackedTarget(targNew);
			}

			// Save Update Target
			Cameras[cameraIndex]->updateCamTarget(camTargets[i]);
			serialOut.sendMessage("T," + std::to_string(cameraIndex) + "," + camTargets[i].to_serial());

			// Speed Tests
			//long a = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - lastUpdate).count());
			//cout << a << "\n";
			//lastUpdate = std::chrono::steady_clock::now();
		}
	}
}


// **********************************************************************************************  //
//    Helper Functions                                                                             //
//																										                                             //
// **********************************************************************************************  //

//creates a mini roi mat (inclusive) of boundaries
/// <summary> roi (Region of Interest) returns a mat clipped to input points </summary>
/// <param name="ptMin"> inclusive of boundary </param>
/// <param name="ptMax"> inclusive of boundary </param>
cv::Mat roiMat(cv::Mat totalMat, cv::Point2i& ptMin, cv::Point2i& ptMax)
{
	//bound to protect from OB of totalMatrix
	ptMin.y = std::max(0, ptMin.y);
	ptMin.x = std::max(0, ptMin.x);
	ptMax.y = std::min(totalMat.rows - 1, ptMax.y);
	ptMax.x = std::min(totalMat.cols - 1, ptMax.x);

	if (ptMax.y < ptMin.y || ptMax.x < ptMin.x) { std::cout << "Error:\n"; }

	Mat miniMat = Mat(cv::Size(ptMax.x - ptMin.x + 1, ptMax.y - ptMin.y + 1), CV_8UC1);
	for (int i = ptMin.x; i <= ptMax.x; i++)
	{
		for (int ii = ptMin.y; ii <= ptMax.y; ii++)
		{
			miniMat.at<cv::uint8_t>(ii - ptMin.y, i - ptMin.x) = totalMat.at<cv::uint8_t>(ii, i);
		}
	}
	return miniMat;
}
