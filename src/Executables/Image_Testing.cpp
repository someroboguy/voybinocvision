

//CPP libaries
#include <stdio.h>
#include <mutex>
#include <thread>

//Included libraries
//Note: these headers have to be before any opencv due to a namespace collision (could probably be fixed)

#include "ApiController.h"


#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

static const int unusedBinocVis = 0;
//Local Libraries
#include "SimpleViewer.h"
#include "StopWatch.h"
#include "StorageObjects.h"
#include "Search_2D.h"


std::vector<std::string> cameraIDs;

std::vector<AVT::VmbAPI::Stream::ApiController> cameras;


//declarations
cv::Mat roiMat(cv::Mat totalMat, cv::Point2i &ptMin, cv::Point2i &ptMax);
void recoverAllAttachedCameras();
bool startupSingleCamera(int count);
void closeCameras();


/// Global variables

Mat src, src_gray;
Mat dst, detected_edges;

int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 250;
int highThreshold;
int const max_highThreshold = 250;
int ratio = 3;
int kernel_size = 3;
char* window_name = "Edge Map";

/**
* @function CannyThreshold
* @brief Trackbar callback - Canny thresholds input with a ratio 1:3
*/
void CannyThreshold(int, void*)
{
	/// Reduce noise with a kernel 3x3
	blur(src_gray, detected_edges, Size(3, 3));

	/// Canny detector
	Canny(detected_edges, detected_edges, lowThreshold, highThreshold, kernel_size, true);

	/// Using Canny's output as a mask, we display our result
	//dst = Scalar::all(0);

	//src.copyTo(dst, detected_edges);
	imshow(window_name, detected_edges);
}


int main(int argc, char** argv)
{
	cameraIDs.push_back("DEV_1AB2280005C5");
	cameraIDs.push_back("DEV_1AB2280005C6");

	std::string userInput;
	std::cout << "choose camera 0,1:" << std::endl;
	std::cin >> userInput;
	int cameraIndex = std::stoi(userInput);

	recoverAllAttachedCameras();
	startupSingleCamera(cameraIndex);

	std::string testType;
	std::cout << "E:edge, P:pipeline" << std::endl;
	std::cin >> testType;
	

	while (true)
	{
		while (!cameras[cameraIndex].newImageAvailable()) { Sleep(50); }
		cv::Mat image = cameras[cameraIndex].getLastImage();
		if (testType == "E")
		{
			dst.create(image.size(), image.type());
			src = image.clone();
			src_gray = image.clone();
			namedWindow("srcImage", CV_WINDOW_AUTOSIZE);
			imshow("srcImage", src);
			namedWindow(window_name, CV_WINDOW_AUTOSIZE);
			/// Create a Trackbar for user to enter threshold
			createTrackbar("Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold);
			createTrackbar("Max Threshold:", window_name, &highThreshold, max_highThreshold, CannyThreshold);
			/// Show the image
			CannyThreshold(0, 0);
			/// Wait until user exit program by pressing a key
			waitKey(0);
		}
		else
		{
			Search_2D fastSearch = Search_2D(image.cols, image.rows);
			//std::vector<circle2D> circles = fastSearch.circlePipelineSlow(image, 20, 45,0.25,30,60);
		}
		
		std::cin >> userInput;
		if (userInput == "x") { break; }
	}

	closeCameras();

	system("pause");

	return 0;
}

bool startupSingleCamera(int index)
{
	cameras.clear();
	VmbErrorType err = VmbErrorSuccess;

	cameras.push_back(AVT::VmbAPI::Stream::ApiController());
	cameras.push_back(AVT::VmbAPI::Stream::ApiController());
	//AVT::VmbAPI::Stream::ProgramConfig Config;
	err = cameras[index].StartUp();
	if (VmbErrorSuccess != err) { std::cout << "startup " << std::to_string(index) << " failed"; system("pause"); return false; }
	//Config.setCameraID(cameraIDs.at(index));
	err = cameras[index].StartContinuousImageAcquisition(cameraIDs.at(index));
	if (VmbErrorSuccess != err) { std::cout << "continuous capture " << std::to_string(index) << " failed\n"; system("pause"); return false; }

	return true;
}

void closeCameras()
{
	VmbErrorType err = VmbErrorSuccess;
	for (int i = 0; i < cameras.size(); i++)
	{
		err = cameras[i].StopContinuousImageAcquisition();
		if (VmbErrorSuccess != err) { std::cout << "continuous capture close 1 failed\n"; }
	}
	for (int i = 0; i < cameras.size(); i++)
	{
		cameras[i].ShutDown();
	}
}

void recoverAllAttachedCameras()
{
	std::cout << "Camera recovered started...\n";
	VmbErrorType err = VmbErrorSuccess;
	AVT::VmbAPI::Stream::ApiController apiController;

	err = apiController.StartUp();
	if (VmbErrorSuccess != err) { std::cout << "startup failed\n"; system("pause"); return; }
	else { std::cout << "startup succeeded" << std::endl; }

	AVT::VmbAPI::CameraPtrVector activeCameras = apiController.GetCameraList();
	if (activeCameras.size() < 1) { std::cout << "no cameras found\n"; system("pause"); return; }
	else { std::cout << "cameras found: " << std::to_string(activeCameras.size()) << std::endl; }

	for (int i = 0; i < activeCameras.size(); i++)
	{
		AVT::VmbAPI::CameraPtr pCam = activeCameras[i];
		err = pCam->Open(VmbAccessModeFull);
		if (VmbErrorSuccess != err) { std::cout << "open camera " << std::to_string(i) << ", error: " << err << " failed\n"; continue; }
		else { std::cout << "camera opened" << std::endl; }

		std::string cameraId;
		err = pCam->GetID(cameraId);
		if (VmbErrorSuccess != err) { std::cout << "get id " << std::to_string(i) << " failed\n"; }
		else { std::cout << "camera opened: " << cameraId << std::endl; }

		std::string xmlFile = "../../resources/CameraConfigs/";
		xmlFile += cameraId;
		xmlFile += ".xml";
		//TODO:temp
		//if (i == 0) { xmlFile += "5c5_10hz.xml"; }
		//if (i == 1) { xmlFile += "5c6_10hz.xml"; }

		err = pCam->LoadCameraSettings(xmlFile);
		if (VmbErrorSuccess != err) { std::cout << "failed settings " << std::to_string(i) << " load\n"; }
		else { std::cout << "settings loaded from :" << xmlFile << std::endl; }

		pCam->StopContinuousImageAcquisition();
		pCam->Close();
	}

	apiController.ShutDown();
	std::cout << "Camera recovered finished!\n";
}
