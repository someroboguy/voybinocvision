#include "BoostSoc.h"

void BoostSoc::serverConnect(int _port, bool blocking, bool autoReconnectOnDrop)
{
	port = _port;
	autoReconnect = autoReconnectOnDrop;
	connectionThread = std::thread([this] {connectionManagement_DoWork(0); });

	if (blocking)
	{
		while (!connected)
		{
			Sleep(500);
		}
	}
}

void BoostSoc::clientConnect(int _port, std::string _ip, bool blocking, bool autoReconnectOnDrop)
{
	port = _port;
	ip = _ip;
	autoReconnect = autoReconnectOnDrop;
	connectionThread = std::thread([this] {connectionManagement_DoWork(1); });

	if (blocking)
	{
		while (!connected)
		{
			Sleep(500);
		}
	}
}

void BoostSoc::disconnectSocket()
{
	connectionKillFlag = true;
	try
	{
		connectedSocket.shutdown(boost::asio::socket_base::shutdown_type::shutdown_both);
		connectedSocket.close();
	}
	catch (...) {}

	if (connectionThread.joinable())
		connectionThread.join();
}

std::vector<std::string> BoostSoc::getMessages(int count, bool flush)
{
	std::vector<std::string> msgs = std::vector<std::string>();

	if (!connected || (count < 1 && count != -1)) { return msgs; }

	sockMtx.lock();

	if (count == -1) { count = inBuffer.size(); }
	
	if (inBuffer.size() > 0)
	{
		for (int i = 0; i < count && i < inBuffer.size(); i++)
		{
			msgs.push_back(inBuffer.at(i));
		}

		if (flush) { inBuffer.clear(); }
		else
		{
			inBuffer.erase(inBuffer.begin(), inBuffer.begin()+ std::min(count-1, (int)(inBuffer.size() - 1)));
		}
	}
	sockMtx.unlock();
	return msgs;
}

std::string BoostSoc::getNextMessage()
{
	std::string msg = "";
	if (!connected) { return msg; }
	
	sockMtx.lock();
	if (inBuffer.size() > 0)
	{
		msg = inBuffer.at(0);
		inBuffer.erase(inBuffer.begin());
	}
	sockMtx.unlock();
	return msg;
}

std::string BoostSoc::getLatestMessage(bool flush)
{
	std::string msg = "";
	if (!connected) { return msg; }
	
	sockMtx.lock();
	if (inBuffer.size() > 0)
	{
		msg = inBuffer.at(inBuffer.size() - 1);
		if (flush) { inBuffer.clear(); }
		else { inBuffer.pop_back(); }
	}
	sockMtx.unlock();
	return msg;
}

bool BoostSoc::sendMessage(std::string message, bool asap)
{
	if (!connected) { return false; }

	sockMtx.lock();
	if (asap)
	{
		try
		{
			message = message + specialChar;
			boost::asio::write(connectedSocket, boost::asio::buffer(message.c_str(), message.length()));
		}
		catch (std::exception& e)
		{
			if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
			connected = false;
			return false;
		}
	}
	else
	{
		outBuffer.push_back(message);
	}
	sockMtx.unlock();
	return true;
}

void BoostSoc::SerialManagement_DoWork()
{
	while (!socketManagementKillFlag)
	{
		int curPeriod = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - mainClock).count();
		while (heartbeatPeriod > curPeriod)
		{
			if (heartbeatPeriod > 10000) //If the requested period is less than Window's scheduler period, we need to hardlock a loop to ensure it triggers on-time
			{
				//if period is greater than 10ms we are fine to sleep
				Sleep((heartbeatPeriod - curPeriod)/1000);
				break;
			}
			curPeriod = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - mainClock).count();
		}
		mainClock = std::chrono::steady_clock::now();
		
		if (!connected || listening) { continue; }

		try
		{
			//TODO::may want to be more specific with mutex lock to improve performance
			sockMtx.lock();
			//write outgoing messages or heartbeat if none
			if (outBuffer.size() > 0)
			{
				for (int i = 0; i < outBuffer.size(); i++)
				{
					std::string delimitedMessage = outBuffer[i] + specialChar;
					boost::asio::write(connectedSocket, boost::asio::buffer(delimitedMessage.c_str(), delimitedMessage.length()));
				}
				outBuffer.clear();
			}
			else
			{
				boost::asio::write(connectedSocket, boost::asio::buffer(specialChar.c_str(), specialChar.length()));
			}

			//read incoming messages (ignoring heartbeats)
			std::size_t count = connectedSocket.available();
			if (count > 0)
			{
				char *inMessage = new char[count];
				boost::asio::read(connectedSocket, boost::asio::buffer(inMessage, count));
				std::string strMessage = std::string(inMessage, count);
				delete[] inMessage;

				std::vector<std::string> toks = splitStr(strMessage, specialChar[0]);
				for (int i = 0; i < toks.size(); i++)
				{
					if(toks.at(i).size()>0)
					{
						inBuffer.push_back(toks.at(i));
					}
				}
			}
			sockMtx.unlock();
		}
		catch (std::exception& e)
		{
			connected = false;
			sockMtx.unlock();
		}
	}
}

void BoostSoc::serverConnect()
{
	listening = true;
	connectedSocket = boost::asio::ip::tcp::socket(io_service);
	try
	{
		using boost::asio::ip::tcp;
		tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
		a.accept(connectedSocket);
		connected = true;
	}
	catch (std::exception& e)
	{
		if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
		connected = false;
	}
	listening = false;
}

void BoostSoc::clientConnect()
{
	listening = true;
	connectedSocket = boost::asio::ip::tcp::socket(io_service);
	while (!connected && !connectionKillFlag)
	{
		try
		{
			using boost::asio::ip::tcp;
			tcp::resolver resolver(io_service);
			tcp::resolver::query query(tcp::v4(), ip, std::to_string(port));
			tcp::resolver::iterator iterator = resolver.resolve(query);
			boost::asio::connect(connectedSocket, iterator);
			connected = true;
		}
		catch (std::exception& e)
		{
			if (debugPrints) { std::cerr << "Exception: " << e.what() << "\n"; }
			connected = false;
		}
		Sleep(500);
	}
	listening = false;
}

void BoostSoc::connectionManagement_DoWork(int socType)
{
	while (!connectionKillFlag)
	{
		Sleep(500);
		if (listening || connected) { continue; }

		if (socType == 0)//server
		{
			this->serverConnect();
		}
		else//client
		{
			this->clientConnect();
		}

		if (!autoReconnect) { break; }//end connection thread
	}
}