#pragma once
#include <cstdlib>
#include <iostream>
#include <boost/asio.hpp>
#include <chrono>
#include <thread>
#include <mutex>

/**Basic boost socket implementation for client and server side communications
*";" is a special char used for heartbeat and message delimitation.  Modify global "specialChar" if neccesary
*All cout stream prints should be gated behind debugPrints flag.
*/
class BoostSoc
{
public:
	bool debugPrints = false;

	BoostSoc(bool _debugPrints = false, int serialPeriodMs = 50) :connectedSocket(io_service) 
	{ 
		debugPrints = _debugPrints;
		heartbeatPeriod = serialPeriodMs * 1000; //convert to uS
		mainClock = std::chrono::steady_clock::now();
		serialThread = std::thread([this] {SerialManagement_DoWork(); });
	}
	~BoostSoc()
	{
		socketManagementKillFlag = true;
		if (serialThread.joinable())
			serialThread.join();
		connectionKillFlag = true;

		try
		{
			connectedSocket.shutdown(boost::asio::socket_base::shutdown_type::shutdown_both);
			connectedSocket.close();
		}
		catch (...) {}

		if (connectionThread.joinable())
			connectionThread.join();
	}

	////Connection management functions////
	void serverConnect(int _port, bool blocking, bool autoReconnectOnDrop = true);
	void clientConnect(int _port, std::string _ip, bool blocking, bool autoReconnectOnDrop = true);
	void disconnectSocket();

	////Incoming and outgoing message functions////

	/**Returns a list of messages from the incoming read message buffer
	*count: -1 will read all messages from buffer, numbers greater than 0 will read the respective nuber of messages in order that they were received
	*flush: if true, will delete all unread (after count pulled) messages from inBuffer
	*/
	std::vector<std::string> getMessages(int count = -1, bool flush = false);

	//return next message from inbuffer
	std::string getNextMessage();

	//returns most recent message from inbuffer and clears all others
	std::string getLatestMessage(bool flush = true);

	//returns number of messages that are in the inbuffer
	int getUnreadMessageCount() { return inBuffer.size(); }

	/**Sends a message to the connected socket
	*asap: if asap true, will send the message immediately (blocking), otherwise will add message to outbuffer to be sent next serial loop
	*/
	bool sendMessage(std::string message, bool asap=false);

	bool getConnected() { return connected; }
	bool getListening() { return listening; }
	
	/**Returns current conneciton status of the socket*/
	std::string getStatus()
	{
		if (listening) { return "listening"; }
		else if (connected) { return "connected"; }
		else { return "broken"; }
	}

	static std::vector<std::string> splitStr(std::string msg, char delimiter)
	{
		std::vector<std::string> result;
		const char *str = msg.c_str();
		char c = delimiter;
		do
		{
			const char *begin = str;
			while (*str != c && *str)
				str++;

			result.push_back(std::string(begin, str));
		} while (0 != *str++);
		return result;
	}

private:
	bool socketManagementKillFlag = false;
	bool connectionKillFlag = false;
	int port;
	std::string ip;
	bool autoReconnect = true;
	std::string specialChar = ";";
	int heartbeatPeriod; //uS

	std::chrono::time_point<std::chrono::steady_clock> mainClock;

	boost::asio::io_service io_service;
	boost::asio::ip::tcp::socket connectedSocket;
	bool connected = false;
	bool listening = false;

	std::thread connectionThread;
	std::thread serialThread;

	std::mutex sockMtx;

	std::vector<std::string> inBuffer = std::vector<std::string>();
	std::vector<std::string> outBuffer = std::vector<std::string>();

	void SerialManagement_DoWork();
	void serverConnect();
	void clientConnect();
	void connectionManagement_DoWork(int socType);
};